﻿using SQLite.CodeFirst;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library.APIs.CurrencyRatesModel;
using World_Library.SERVICES;

namespace World_Library.DATABASE.DBCONTEXTS
{
    /// <summary>
    /// Rate Database Context Controller
    /// </summary>
    internal class RateDBContext : DbContext
    {
        /// <summary>
        /// Rates Table
        /// </summary>
        public DbSet<Rate> Rates { get; set; }

        /// <summary>
        ///Contructor of Rate Database Context 
        /// </summary>
        internal RateDBContext() : base("RateDatabase")
        {
        }

        /// <summary>
        /// Override when database is being created
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var SQLiteInitializer = new SqliteCreateDatabaseIfNotExists<RateDBContext>(modelBuilder);
            Database.SetInitializer(SQLiteInitializer);
        }

        /// <summary>
        /// Cleans all tables from database
        /// </summary>
        /// <returns></returns>
        internal async Task<ServiceResponseModel> ClearDatabase()
        {

            await this.Database.ExecuteSqlCommandAsync(

                @"DELETE FROM Rates"

                );


            await this.Database.ExecuteSqlCommandAsync(

                TransactionalBehavior.DoNotEnsureTransaction,
                @"VACUUM"

                );


            return new ServiceResponseModel() { Success = true };
        }
    }
}
