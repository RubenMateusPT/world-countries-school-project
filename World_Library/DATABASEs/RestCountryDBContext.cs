﻿using SQLite.CodeFirst;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library.APIs.RestCountriesModel;
using World_Library.APIs.RestCountriesModel.SubModels;
using World_Library.SERVICES;
using World_Library.SERVICES.MODELS;

namespace World_Library.DATABASE.DBCONTEXTS
{
    /// <summary>
    /// Rest Country Database Controller
    /// </summary>
    internal class RestCountryDBContext : DbContext
    {
        /// <summary>
        /// Rest Countries Table
        /// </summary>
        public DbSet<RESTCountry> RESTCountries { get; set; }

        /// <summary>
        /// Rest Regional Blocs Table
        /// </summary>
        public DbSet<RESTRegionalBloc> RESTRegionalBlocs { get; set; }

        /// <summary>
        /// Rest Languages Table
        /// </summary>
        public DbSet<RESTLanguage> RESTLanguages { get; set; }

        /// <summary>
        /// Rest Currency Table
        /// </summary>
        public DbSet<RESTCurrency> RESTCurrencies { get; set; }

        /// <summary>
        /// Rest TimeZone Table
        /// </summary>
        public DbSet<RESTTimeZone> RESTTimeZones { get; set; }

        /// <summary>
        /// Rest Flag Table
        /// </summary>
        public DbSet<RESTFlag> RESTFlags { get; set; }

        /// <summary>
        /// Contructor of Rest Country Database Context
        /// </summary>
        internal RestCountryDBContext() : base("RestCountryDatabase")
        {
        }

        /// <summary>
        /// Override when database is being created
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var SQLiteInitializer = new SqliteCreateDatabaseIfNotExists<RestCountryDBContext>(modelBuilder);
            Database.SetInitializer(SQLiteInitializer);
        }

        /// <summary>
        /// Cleans all tables from database
        /// </summary>
        /// <returns></returns>
        internal async Task<ServiceResponseModel> ClearDatabase()
        {

            await this.Database.ExecuteSqlCommandAsync(

                @"DELETE FROM RESTCountries"

                );

            await this.Database.ExecuteSqlCommandAsync(

                @"DELETE FROM RESTAltSpellings"

                );

            await this.Database.ExecuteSqlCommandAsync(

                @"DELETE FROM RESTBorders"

                );

            await this.Database.ExecuteSqlCommandAsync(

                @"DELETE FROM RESTCallingCodes"

                );

            await this.Database.ExecuteSqlCommandAsync(

                @"DELETE FROM RESTGPSLocations"

                );

            await this.Database.ExecuteSqlCommandAsync(

                @"DELETE FROM RESTTimeZones"

                );

            await this.Database.ExecuteSqlCommandAsync(

                @"DELETE FROM RESTTopLevelDomains"

                );

            await this.Database.ExecuteSqlCommandAsync(

                @"DELETE FROM RESTCurrencies"

                );

            await this.Database.ExecuteSqlCommandAsync(

                @"DELETE FROM RESTLanguages"

                );

            await this.Database.ExecuteSqlCommandAsync(

                @"DELETE FROM RESTTranslations"

                );

            await this.Database.ExecuteSqlCommandAsync(

                @"DELETE FROM RESTRegionalBlocs"

                );

            await this.Database.ExecuteSqlCommandAsync(

                @"DELETE FROM RESTRegionalBlocOtherAcronyms"

                );

            await this.Database.ExecuteSqlCommandAsync(

                @"DELETE FROM RESTRegionalBlocOtherNames"

                );

            await this.Database.ExecuteSqlCommandAsync(

                @"DELETE FROM RESTFlags"

                );

            await this.Database.ExecuteSqlCommandAsync(

                TransactionalBehavior.DoNotEnsureTransaction,
                @"VACUUM"

                );


            return new ServiceResponseModel() {Success = true };
        }
    }
}
