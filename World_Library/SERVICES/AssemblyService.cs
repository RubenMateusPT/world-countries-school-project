﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using World_Library.APIs.RestCountriesModel;
using World_Library.DATA.CONTROLLERS;
using World_Library.SERVICES.MODELS;

namespace World_Library.SERVICES
{
    /// <summary>
    /// Assembly Service Controller
    /// </summary>
    internal class AssemblyService
    {
        /// <summary>
        /// Creates The DataController needed to work
        /// </summary>
        public DataController DataController { get; set; } = new DataController();

        /// <summary>
        /// Makes a preload of all data needed before creating countries
        /// </summary>
        /// <param name="RestCountries">List of all restcountries from api</param>
        /// <param name="progress">Receives a REportModel</param>
        /// <returns>Returns a unsucessful/sucessful</returns>
        internal async Task<ServiceResponseModel> PreLoad(List<RESTCountry> RestCountries, IProgress<ReportModel> progress)
        {
                Stopwatch sp = new Stopwatch();
                sp.Start();

                ReportModel preloadReport = new ReportModel() {Current = 0, Goal = 6};
                preloadReport.Message = $"A começar o precarregamento das base de dados... [{preloadReport.Current}/{preloadReport.Goal}] \n";
            progress.Report(preloadReport);

            List<Task> TaskController = new List<Task>() {

             Task.Run(() => DataController.RegionController.LoadRegions(RestCountries)),

             Task.Run(() => DataController.SubRegionController.LoadSubRegions(RestCountries)),

             Task.Run(() => DataController.TimeZoneController.LoadCustomTimeZones(RestCountries)),

             Task.Run(() => DataController.CurrencyController.LoadCurrencies(RestCountries)),

             Task.Run(() => DataController.LanguageController.LoadLanguages(RestCountries)),

             Task.Run(() => DataController.RegionalBlocController.LoadRegionalBlocs(RestCountries)),
        };

            await Task.WhenAll(TaskController);

            foreach(Task task in TaskController)
            {
                if (task.IsFaulted)
                {
                    sp.Stop();
                    preloadReport.Current = preloadReport.Goal;
                    preloadReport.Message = $"ERRO: Falha no precarregamento das base de dados [{preloadReport.Current}/{preloadReport.Goal}] ({sp.Elapsed.Seconds} segundos) \n";
                    progress.Report(preloadReport);


                    return new ServiceResponseModel() {
                        EllapsedTime = sp.Elapsed,
                        Success = false,
                        Message = "ERROR: There was an error on preloading databases",
                    };
                }
            }

            sp.Stop();
            preloadReport.Current = preloadReport.Goal;
            preloadReport.Message = $"Fim do precarregamento das base de dados [{preloadReport.Current}/{preloadReport.Goal}] ({sp.Elapsed.Seconds} segundos) \n";
            progress.Report(preloadReport);

            return new ServiceResponseModel()
            {
                EllapsedTime = sp.Elapsed,
                Success = true,
                Message = "Finished preloading databases",
            };

        }

        /// <summary>
        /// Loads all data that requeries Pre-loaded data
        /// </summary>
        /// <param name="restCountries">List of all restcountries from api</param>
        /// <param name="progress">Receives a Reportmodel</param>
        /// <returns>Returns a unsucessful/sucessful</returns>
        internal async Task<ServiceResponseModel> Load(List<RESTCountry> restCountries, IProgress<ReportModel> progress)
        {
            Stopwatch sp = new Stopwatch();
            sp.Start();

            ReportModel loadReport = new ReportModel() { Current = 0, Goal = 1 };
            loadReport.Message = $"A começar o carregameto das base de dados... [{loadReport.Current}/{loadReport.Goal}] \n";
            progress.Report(loadReport);

            try
            {
                await DataController.CountryController.LoadCountries(restCountries, progress);

                sp.Stop();
                loadReport.Current = loadReport.Goal;
                loadReport.Message = $"Fim do carregamento das base de dados [{loadReport.Current}/{loadReport.Goal}] ({sp.Elapsed.Seconds} segundos) \n";
                progress.Report(loadReport);

                return new ServiceResponseModel()
                {
                    EllapsedTime = sp.Elapsed,
                    Success = true,
                    Message = "Finished loading databases",
                };
            }
            catch
            {
                sp.Stop();
                loadReport.Current = loadReport.Goal;
                loadReport.Message = $"ERRO: Falha no carregamento das base de dados [{loadReport.Current}/{loadReport.Goal}] ({sp.Elapsed.Seconds} segundos) \n";
                progress.Report(loadReport);


                return new ServiceResponseModel()
                {
                    EllapsedTime = sp.Elapsed,
                    Success = false,
                    Message = "ERROR: There was an error on loading databases",
                };
            }
        }

        /// <summary>
        /// Cleans up and load all post-data that requeries pre-loaded and loaded data
        /// </summary>
        /// <param name="RestCountries">List of all restcountries from api</param>
        /// <param name="progress">Receives a reportModel</param>
        /// <returns>Returns a unsucessful/sucessful</returns>
        internal async Task<ServiceResponseModel> PostLoad(List<RESTCountry> RestCountries, IProgress<ReportModel> progress)
        {

            Stopwatch sp = new Stopwatch();
            sp.Start();

            ReportModel postloadReport = new ReportModel() { Current = 0, Goal = 3 };
            postloadReport.Message = $"A começar o pós-carregameto das base de dados... [{postloadReport.Current}/{postloadReport.Goal}] \n";
            progress.Report(postloadReport);

            try
            {


                await Task.Run(() => DataController.SubRegionController.LoadRegions());

                List<Task> TaskController = new List<Task>()
            {
                Task.Run(() => DataController.CountryController.LoadCountryBorders(RestCountries) ),
                Task.Run(() => DataController.RegionController.LoadSubRegions() ),
                DataController.CountryController.DownloadFlags(progress),

            };

                await Task.WhenAll(TaskController);


                sp.Stop();
                postloadReport.Current = postloadReport.Goal;
                postloadReport.Message = $"Fim do pós-carregamento das base de dados [{postloadReport.Current}/{postloadReport.Goal}] ({sp.Elapsed.Seconds} segundos) \n";
                progress.Report(postloadReport);

                return new ServiceResponseModel()
                {
                    EllapsedTime = sp.Elapsed,
                    Success = true,
                    Message = "Finished post-loading databases",
                };

            }
            catch
            {
                sp.Stop();
                postloadReport.Current = postloadReport.Goal;
                postloadReport.Message = $"ERRO: Falha noo pós-carregamento das base de dados \n";
                progress.Report(postloadReport);

                return new ServiceResponseModel()
                {
                    EllapsedTime = sp.Elapsed,
                    Success = true,
                    Message = "ERROR: FAile to postload databases",
                };
            }
        }

    }
}
