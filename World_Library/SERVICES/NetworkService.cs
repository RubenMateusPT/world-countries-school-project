﻿namespace World_Library.SERVICES
{
    using System;
    using System.Diagnostics;
    using System.Net;
    using World_Library.SERVICES.MODELS;

    /// <summary>
    /// Network Service (Related to internet connection)
    /// </summary>
    public class NetworkService
    {
        /// <summary>
        /// Checks the connection to internet without reporting
        /// </summary>
        /// <returns>Returns a ResponseModel with Success  or not</returns>
        public ServiceResponseModel CheckConnection()
        {
            return CheckConnection(new Progress<ReportModel>());
        }

        /// <summary>
        /// Checks the connection to the internet and reports it
        /// </summary>
        /// <param name="progress">Report Model</param>
        /// <returns>Return a ResponseModel with Success or not</returns>
        public ServiceResponseModel CheckConnection(IProgress<ReportModel> progress)
        {
                ReportModel reportNetwork = new ReportModel() {Message = "A verificar ligação à internet... \n" };
            reportNetwork.Percentage += 2;
                Stopwatch sp = new Stopwatch();
                sp.Start();


            var client = new WebClient();

            try
            {
                using (client.OpenRead("http://clients3.google.com/generate_204"))
                {
                        sp.Stop();

                        reportNetwork.Message = $"Existe conexão à internet ({sp.Elapsed.Seconds} segundos) \n";
                    reportNetwork.Percentage += 3;
                    progress.Report(reportNetwork);

                    return new ServiceResponseModel()
                    {
                        Message = "Network Connection has been succeful",
                        EllapsedTime = sp.Elapsed,
                        Success = true,
                    };
                }
            }
            catch
            {
                    sp.Stop();

                    reportNetwork.Message = $"Sem ligação disponivel à internet ({sp.Elapsed.Seconds} segundos) \n";
                reportNetwork.Percentage += 3;
                progress.Report(reportNetwork);

                return new ServiceResponseModel()
                {
                    EllapsedTime = sp.Elapsed,
                    Message = "No Network Connection",
                    Success = false,
                };
            }

        }
    }
}
