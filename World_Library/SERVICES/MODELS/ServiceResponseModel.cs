﻿using System;

namespace World_Library.SERVICES
{
    /// <summary>
    /// Response of all Task Methods
    /// </summary>
    public class ServiceResponseModel
    {
        /// <summary>
        /// Service Response Unsuccess/Success
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Service REsponse Custom Message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Service Response Result (if any)
        /// </summary>
        public object Result { get; set; }

        /// <summary>
        /// Service Response EllapsedTime (How much time has passes since the beggining of method)
        /// </summary>
        public TimeSpan EllapsedTime { get; set; }
    }
}
