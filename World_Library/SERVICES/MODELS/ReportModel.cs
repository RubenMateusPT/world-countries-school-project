﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace World_Library.SERVICES.MODELS
{
    /// <summary>
    /// Report Model
    /// </summary>
    public class ReportModel
    {
        /// <summary>
        /// Report Model Message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Report Model Goal (How many things needs to do)
        /// </summary>
        public int Goal { get; set; }

        /// <summary>
        /// Report Model Current (How many has it done relative to goal)
        /// </summary>
        public int Current { get; set; }

        /// <summary>
        /// Report Model Percentage (In what percentage is it)
        /// </summary>
        public double Percentage { get; set; }
    }
}
