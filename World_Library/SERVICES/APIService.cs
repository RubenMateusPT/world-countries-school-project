﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using World_Library.APIs.CurrencyRatesModel;
using World_Library.APIs.RestCountriesModel;
using World_Library.SERVICES;
using World_Library.SERVICES.MODELS;

namespace World_Library.APIs
{
    /// <summary>
    /// API Service 
    /// </summary>
    public class APIService
    {
        /// <summary>
        /// Gets the Json From API
        /// </summary>
        /// <param name="APIModel">Receives APIModel to work with</param>
        /// <param name="progress">REceives a ReportModel</param>
        /// <returns></returns>
        public static async Task<ServiceResponseModel> GetApi(APIModel APIModel, IProgress<ReportModel> progress)
        {
                Stopwatch sp = new Stopwatch();
                sp.Start();

                ReportModel reportApi = new ReportModel() {Message = $"Inicio do download da API: {APIModel.ApiName}... \n" };
                progress.Report(reportApi);

            var client = new HttpClient()
            {
                BaseAddress = new Uri(APIModel.Controller),
            };

            try
            {
                var response = await client.GetAsync(APIModel.Action);

                string result = await response.Content.ReadAsStringAsync();

                sp.Stop();

                reportApi.Message = $"{APIModel.ApiName} terminou o download ({sp.Elapsed.Seconds} segundos) \n";
                progress.Report(reportApi);

                return new ServiceResponseModel()
                {

                    Result = result,
                    EllapsedTime = sp.Elapsed,
                    Success = true,
                    Message = $"{APIModel.ApiName} has been download",

                };
            }
            catch
            {

                    sp.Stop();

                    reportApi.Message = $"ERRO: Não foi possivel obter:{APIModel.ApiName} \n";
                    progress.Report(reportApi);

                return new ServiceResponseModel()
                {
                    EllapsedTime = sp.Elapsed,
                    Success = false,
                    Message = $"ERROR: Failed to obtain api: {APIModel.ApiName}",
                };
            }

        } 
    }
}
