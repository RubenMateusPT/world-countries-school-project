﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using World_Library.APIs;
using World_Library.APIs.CurrencyRatesModel;
using World_Library.APIs.RestCountriesModel;
using World_Library.DATABASE.DBCONTEXTS;
using World_Library.SERVICES.MODELS;

namespace World_Library.SERVICES
{
    /// <summary>
    /// Database Service Controller
    /// </summary>
    internal class DatabaseService
    {
        /// <summary>
        /// Gets all online data needed and updates it's databases
        /// </summary>
        /// <param name="progress">Receives a ReportModel</param>
        /// <returns>Return a Reponse of task</returns>
        internal async Task<ServiceResponseModel> UpdateDatabase(IProgress<ReportModel> progress)
        {
                Stopwatch sp = new Stopwatch();
                sp.Start();

                ReportModel reportDatabase = new ReportModel() {Current=0, Goal= 2 };
                reportDatabase.Message = $"Começo das atualizações de base de dados... [{reportDatabase.Current}/{reportDatabase.Goal}] \n";
            reportDatabase.Percentage = 20;
                progress.Report(reportDatabase);

            List<Task<ServiceResponseModel>> TaskController = new List<Task<ServiceResponseModel>>()
            {
                UpdateRESTCountriesDB(progress),
                UpdateRatesDB(progress),
            };

            await Task.WhenAll(TaskController);

            // Verifies if databases were succesufuly updated
            foreach(Task<ServiceResponseModel> task in TaskController)
            {
                if (!task.Result.Success)
                {
                    sp.Stop();
                    reportDatabase.Message = $"ERRO: Falha nas atualizações das base de dados [{reportDatabase.Current}/{reportDatabase.Goal}] ({sp.Elapsed.Seconds} segundos) \n";
                    progress.Report(reportDatabase);

                    return new ServiceResponseModel()
                    {
                        Success = false,
                        EllapsedTime = sp.Elapsed,
                        Message = "ERROR: Failed to update databases ",
                    };
                }
            }

                sp.Stop();
                reportDatabase.Current = reportDatabase.Goal;
                reportDatabase.Message = $"Fim das atualizações das base de dados [{reportDatabase.Current}/{reportDatabase.Goal}] ({sp.Elapsed.Seconds} segundos) \n";
            reportDatabase.Percentage = 25;
                progress.Report(reportDatabase);

            return new ServiceResponseModel() {
                Success = true,
                EllapsedTime = sp.Elapsed,
                Message = "All databases have been updated",
            };

        }

        /// <summary>
        /// Updates the RestCountries Database
        /// </summary>
        /// <param name="progress">Receives REportModel</param>
        /// <returns>REturn a unsucessful/successfull response</returns>
        private async Task<ServiceResponseModel> UpdateRESTCountriesDB(IProgress<ReportModel> progress)
        {

            using (RestCountryDBContext RESTCountriesDB = new RestCountryDBContext())
            {
                var RestCountryApi = await APIService.GetApi(new APIModel("RestCountries", "http://restcountries.eu", "/rest/v2/all"), progress);

                if (RestCountryApi.Success)
                {
                    List<RESTCountry> RestCountryList = JsonConvert.DeserializeObject<List<RESTCountry>>((string)RestCountryApi.Result);

                    RESTCountriesDB.RESTCountries.AddRange(RestCountryList);


                    await RESTCountriesDB.ClearDatabase();

                    await RESTCountriesDB.SaveChangesAsync();

                    progress.Report(new ReportModel() { Message = $"A base de dados RestCountries foi atualizada... \n" });

                    return new ServiceResponseModel() {
                        Success = true,
                        Message = "RestCountries has been updated",
                    };
                }

                progress.Report(new ReportModel() { Message = $"A base de dados RestCountries falhou a ser atualizada... \n" });

                return new ServiceResponseModel() {
                    Success = false,
                    Message = "ERROR: RestCountries has failed to update",
                };
            }            
        }

        /// <summary>
        /// Updates The Rates Database
        /// </summary>
        /// <param name="progress">REceives a ReportModel</param>
        /// <returns>Return a unsucessful/successfull response</returns>
        private async Task<ServiceResponseModel> UpdateRatesDB(IProgress<ReportModel> progress)
        {


            using (RateDBContext RatesDB = new RateDBContext())
            {
                var RateApi = await APIService.GetApi(new APIModel("RafaelRates", "http://rafasaints-001-site3.ctempurl.com", "/api/rates"), progress);

                if (RateApi.Success)
                {
                    List<Rate> RateList = JsonConvert.DeserializeObject<List<Rate>>((string)RateApi.Result);

                    RatesDB.Rates.AddRange(RateList);


                    await RatesDB.ClearDatabase();


                    await RatesDB.SaveChangesAsync();

                    progress.Report(new ReportModel() {Message = $"A base de dados Rate foi atualizada... \n" });

                    return new ServiceResponseModel()
                    {

                        Success = true,
                        Message = "Rates has been updated",
                    };
                }

                progress.Report(new ReportModel() { Message = $"A base de dados Rate falhou a ser atualizada... \n" });

                return new ServiceResponseModel()
                {
                    Success = false,
                    Message = "ERROR: Rates has failed to update",
                };
            }
        }

        /// <summary>
        /// Loads Data from Database
        /// </summary>
        /// <param name="progress">Receives a ReportModel</param>
        /// <returns>Return a unsucessfull/successful response</returns>
        internal async Task<ServiceResponseModel> LoadDatabase(IProgress<ReportModel> progress)
        {
                Stopwatch sp = new Stopwatch();
                sp.Start();
            ReportModel databaseReport = new ReportModel();

            AssemblyService assemblyService = new AssemblyService();
            ServiceResponseModel assemblyResponse = new ServiceResponseModel() {Success = true };

            List<RESTCountry> RestCountries = new List<RESTCountry>();

            // Gets all tables needed from RestCountries database
            using (RestCountryDBContext RCDB = new RestCountryDBContext())
            {
                RestCountries = RCDB.RESTCountries.Include("topLevelDomain")
                                                                    .Include("callingCodes")
                                                                    .Include("altSpellings")
                                                                    .Include("timezones")
                                                                    .Include("borders")
                                                                    .Include("currencies")
                                                                    .Include("languages")
                                                                    .Include("regionalBlocs")
                                                                    .Include("flag")
                                                                    .Include("translations")
                                                                    .Include("latlng")
                                                                    .ToList();
            }


            assemblyResponse = await assemblyService.PreLoad(RestCountries, progress);

            if (!assemblyResponse.Success)
            {

                return new ServiceResponseModel() {
                    Success = false,
                    EllapsedTime = sp.Elapsed,
                    Message = "ERROR: Failed to preload databases"
                };
            }



            assemblyResponse = await assemblyService.Load(RestCountries, progress);

            if (!assemblyResponse.Success)
            {
                return new ServiceResponseModel()
                {
                    Success = false,
                    EllapsedTime = sp.Elapsed,
                    Message = "ERROR: Failed to load databases"
                };
            }

            await assemblyService.PostLoad(RestCountries, progress);
            if (!assemblyResponse.Success)
            {
                return new ServiceResponseModel()
                {
                    Success = false,
                    EllapsedTime = sp.Elapsed,
                    Message = "ERROR: Failed to post-load databases"
                };
            }

            
            sp.Stop();
            databaseReport.Message = $"O carregamento das base de dados foi feito com successo ({sp.Elapsed.Seconds} segundos) \n";
            progress.Report(databaseReport);

            return new ServiceResponseModel()
            {
                Success = true,
                EllapsedTime = sp.Elapsed,
                Message = "Database has been loaded"
            };
        }


    }
}
