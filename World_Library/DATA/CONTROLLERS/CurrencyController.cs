﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library.APIs.CurrencyRatesModel;
using World_Library.APIs.RestCountriesModel;
using World_Library.APIs.RestCountriesModel.SubModels;
using World_Library.DATA.MODELS.World.CountrySUBMODELS;
using World_Library.DATABASE.DBCONTEXTS;

namespace World_Library.DATA.CONTROLLERS
{

    /// <summary>
    /// Controller of Currency Model
    /// </summary>
    internal class CurrencyController
    {
        /// <summary>
        /// Finds all unique Currencies, and it's taxrate to create unique own Currency Model
        /// </summary>
        /// <param name="restCountries">List of restcountries received from API</param>
        internal void LoadCurrencies(List<RESTCountry> restCountries)
        {
            using (RestCountryDBContext RCDB = new RestCountryDBContext())
            {
                List<RESTCurrency> RestCurrencies = RCDB.RESTCurrencies.ToList();

                foreach (RESTCurrency restCurrency in RestCurrencies)
                {
                    string currencyName = restCurrency.name;
                    string currencyCode = restCurrency.code;

                    if (restCurrency.name == null)
                    {
                        currencyName = "null";
                    }
                    if (restCurrency.code == null)
                    {
                        currencyCode = "null";
                    }

                    if (!WorldController.Currencies.Exists(x => x.Code == currencyCode && x.Name == currencyName))
                    {
                        Currency newCurrency = new Currency() { Code = currencyCode, Name = restCurrency.name, Symbol = restCurrency.symbol };

                        using (RateDBContext RDB = new RateDBContext())
                        {
                            List<Rate> Rates = RDB.Rates.ToList();

                            foreach (Rate rate in Rates)
                            {
                                string rateName = rate.Name;

                                if (rate.Name == null)
                                {
                                    rateName = "null";
                                }

                                if (rateName.ToUpper() == currencyName.ToUpper())
                                {
                                    newCurrency.TaxRate = rate.TaxRate;
                                }

                            }
                        }

                        WorldController.Currencies.Add(newCurrency);
                    }

                }

            }
        }

    }
}
