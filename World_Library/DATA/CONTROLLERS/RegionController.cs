﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library.APIs.RestCountriesModel;
using World_Library.DATA.MODELS;
using World_Library.DATA.MODELS.World.CountrySUBMODELS;

namespace World_Library.DATA.CONTROLLERS
{
    /// <summary>
    /// Controller of Region
    /// </summary>
    internal class RegionController
    {
        /// <summary>
        /// Finds all unique REgion, and it's taxrate to create unique own REgion Model
        /// </summary>
        /// <param name="restCountries">List of restcountries received from API</param>
        internal void LoadRegions(List<RESTCountry> restCountries)
        {
            

            foreach (RESTCountry restCountry in restCountries)
            {

                if (!WorldController.Regions.Exists(x => x.Name.ToUpper() == restCountry.region.ToUpper()))
                {
                    WorldController.Regions.Add(new Region() { Name = restCountry.region.ToUpper() });
                }
            }
        }

        /// <summary>
        /// Adds all subregion to it's Region
        /// </summary>
        internal void LoadSubRegions()
        {
            foreach(Region region in WorldController.Regions)
            {
                region.SubRegions = WorldController.SubRegions.Where(x => x.Region == region).ToList();
            }
        }
    }
}
