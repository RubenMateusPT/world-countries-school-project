﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library.APIs.RestCountriesModel;
using World_Library.APIs.RestCountriesModel.SubModels;
using World_Library.DATA.MODELS;
using World_Library.DATA.MODELS.World.CountrySUBMODELS;
using World_Library.DATABASE.DBCONTEXTS;

namespace World_Library.DATA.CONTROLLERS
{
    /// <summary>
    /// Controller of Language Model
    /// </summary>
    internal class LanguageController
    {
        /// <summary>
        /// Finds all unique Language, to create unique own Language Model
        /// </summary>
        /// <param name="restCountries">List of restcountries received from API</param>
        internal void LoadLanguages(List<RESTCountry> restCountries)
        {
            using (RestCountryDBContext RCDB = new RestCountryDBContext())
            {
                List<RESTLanguage> RestLanguages = RCDB.RESTLanguages.ToList();

                foreach (RESTLanguage restLanguage in RestLanguages)
                {
                    if (!WorldController.Languages.Exists(x => x.Name.ToUpper() == restLanguage.name.ToUpper()))
                    {
                        WorldController.Languages.Add(new Language() { Name = restLanguage.name, NativeName = restLanguage.nativeName, Iso639_1 = restLanguage.iso639_1, Iso639_2 = restLanguage.iso639_2 });
                    }
                }
            }
        }

    }
}
