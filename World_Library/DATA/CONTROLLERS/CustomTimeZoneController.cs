﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library.APIs.RestCountriesModel;
using World_Library.APIs.RestCountriesModel.SubModels;
using World_Library.DATA.MODELS.World.CountrySUBMODELS;
using World_Library.DATABASE.DBCONTEXTS;

namespace World_Library.DATA.CONTROLLERS
{
    /// <summary>
    /// Controller of Custom TimeZone
    /// </summary>
    internal class CustomTimeZoneController
    {
        /// <summary>
        /// Finds all unique TimeZones,  to create unique own TimeZone Model
        /// </summary>
        /// <param name="restCountries">List of restcountries received from API</param>
        internal void LoadCustomTimeZones(List<RESTCountry> restCountries)
        {

            using (RestCountryDBContext RCDB = new RestCountryDBContext())
            {
                List<RESTTimeZone> RestTimeZones = RCDB.RESTTimeZones.ToList();

                foreach (RESTTimeZone restTimezone in RestTimeZones)
                {
                    if (!WorldController.CustomTimeZones.Exists(x => x.ToString() == restTimezone.ToString()))
                    {
                        WorldController.CustomTimeZones.Add(new CustomTimeZone() { TimeZoneCode = restTimezone.TimeZoneCode, Positive = restTimezone.Positive, MilisecondsOffSet = restTimezone.MilisecondsOffSet });
                    }
                }
            }
        }


    }

}
