﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace World_Library.DATA.CONTROLLERS
{
    /// <summary>
    /// Controller of All Data
    /// </summary>
    internal class DataController
    {
        /// <summary>
        /// Creates Controller of Country
        /// </summary>
        public CountryController CountryController { get; set; } = new CountryController();

        /// <summary>
        /// Creates Controller of Language
        /// </summary>
        public LanguageController LanguageController { get; set; } = new LanguageController();

        /// <summary>
        /// Creates Controller of RegionalBloc
        /// </summary>
        public RegionalBlocController RegionalBlocController { get; set; } = new RegionalBlocController();

        /// <summary>
        /// Creates Controller of Currency
        /// </summary>
        public CurrencyController CurrencyController { get; set; } = new CurrencyController();

        /// <summary>
        /// Creates Controller of Custom TimeZones
        /// </summary>
        public CustomTimeZoneController TimeZoneController { get; set; } = new CustomTimeZoneController();

        /// <summary>
        /// Creates Controller of Regions
        /// </summary>
        public RegionController RegionController { get; set; } = new RegionController();

        /// <summary>
        /// Creates Controller of SubRegions
        /// </summary>
        public SubRegionController SubRegionController { get; set; } = new SubRegionController();
    }
}
