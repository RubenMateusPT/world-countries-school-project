﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library.APIs.RestCountriesModel;
using World_Library.APIs.RestCountriesModel.SubModels;
using World_Library.APIs.RestCountriesModel.SubModels.RegionalBlocSubModels;
using World_Library.DATA.MODELS.World.CountrySUBMODELS;
using World_Library.DATABASE.DBCONTEXTS;

namespace World_Library.DATA.CONTROLLERS
{
    /// <summary>
    /// Controller of RegionalBloc
    /// </summary>
    internal class RegionalBlocController
    {
        /// <summary>
        /// Finds all unique RegionalBloc, to create unique own RegionalBloc Model
        /// </summary>
        /// <param name="restCountries">List of restcountries received from API</param>
        internal void LoadRegionalBlocs(List<RESTCountry> restCountries)
        {
            using (RestCountryDBContext RCDB = new RestCountryDBContext())
            {
                List<RESTRegionalBloc> RestRegionalBlocs = RCDB.RESTRegionalBlocs.Include("otherAcronyms")
                                                                                 .Include("otherNames")
                                                                                 .ToList();

                foreach (RESTRegionalBloc restRegionalBloc in RestRegionalBlocs)
                {
                    if (!WorldController.RegionalBlocs.Exists(X => X.Name.ToUpper() == restRegionalBloc.name.ToUpper()))
                    {
                        RegionalBloc newRegionalBloc = new RegionalBloc()
                        {
                            Name = restRegionalBloc.name,
                            Acronym = restRegionalBloc.acronym,
                            OtherNames = new List<string>(),
                            OtherAcronyms = new List<string>(),
                        };

                        if (restRegionalBloc.otherNames.Count != 0)
                        {
                            foreach (RESTRegionalBlocOtherName rESTRegionalBlocOtherName in restRegionalBloc.otherNames)
                            {
                                newRegionalBloc.OtherNames.Add(rESTRegionalBlocOtherName.OtherName);
                            }
                        }

                        if (restRegionalBloc.otherAcronyms.Count != 0)
                        {
                            foreach (RESTRegionalBlocOtherAcronym rESTRegionalBlocOtherAcronym in restRegionalBloc.otherAcronyms)
                            {
                                newRegionalBloc.OtherAcronyms.Add(rESTRegionalBlocOtherAcronym.OTherAcronym);
                            }
                        }

                        WorldController.RegionalBlocs.Add(newRegionalBloc);
                    }
                }
            }
        }
    }
}
