﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library.APIs.RestCountriesModel;
using World_Library.DATA.MODELS;
using World_Library.DATA.MODELS.World.CountrySUBMODELS;

namespace World_Library.DATA.CONTROLLERS
{
    /// <summary>
    /// Controller of SubREgion
    /// </summary>
    internal class SubRegionController
    {
        /// <summary>
        /// Finds all unique SubRegions to create unique own SubRegion Model
        /// </summary>
        /// <param name="restCountries">List of restcountries received from API</param>
        internal void LoadSubRegions(List<RESTCountry> restCountries)
        {

            foreach (RESTCountry restCountry in restCountries)
            {
                if (!WorldController.SubRegions.Exists(x => x.Name.ToUpper() == restCountry.subregion.ToUpper()))
                {
                    WorldController.SubRegions.Add(new SubRegion() { Name = restCountry.subregion });
                }
            }
        }

        /// <summary>
        /// Add REgion to correct Subregion
        /// </summary>
        internal void LoadRegions()
        {
            foreach(SubRegion subRegion in WorldController.SubRegions)
            {
                List<Country> Countries = WorldController.Countries.Where(x => x.SubRegion == subRegion).ToList();

                foreach(Country country in Countries)
                {
                    subRegion.Region = country.Region;
                }
            }
        }
    }
}
