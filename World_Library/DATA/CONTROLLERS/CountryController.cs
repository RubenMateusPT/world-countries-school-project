﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using World_Library.APIs.RestCountriesModel;
using World_Library.APIs.RestCountriesModel.SubModels;
using World_Library.DATA.MODELS;
using World_Library.DATA.MODELS.World.CountrySUBMODELS;
using World_Library.SERVICES.MODELS;

namespace World_Library.DATA.CONTROLLERS
{
    /// <summary>
    /// Controller of Country Model
    /// </summary>
    internal class CountryController
    {
        /// <summary>
        /// Creates own custom Country Model for each RestCountry received
        /// </summary>
        /// <param name="restCountries">Receives list of RESTCountries obtained from API</param>
        /// <param name="progress">Receives an ReportModel</param>
        /// <returns></returns>
        internal async Task LoadCountries(List<RESTCountry> restCountries, IProgress<SERVICES.MODELS.ReportModel> progress)
        {

            ReportModel countryReport = new ReportModel() { Current = 0, Goal = restCountries.Count() };
            countryReport.Message = $"A começar o carregameto dos paises... [{countryReport.Current}/{countryReport.Goal}] \n";
            progress.Report(countryReport);

            foreach (RESTCountry restCountry in restCountries)
            {
                Country newCountry = new Country()
                {
                    Name = restCountry.name,
                    TopLevelDomains = new List<string>(),
                    Alpha2Code = restCountry.alpha2Code,
                    Alpha3Code = restCountry.alpha3Code,
                    CallingCodes = new List<string>(),
                    Capital = restCountry.capital,
                    AltSpellings = new List<string>(),
                    Region = new Region(),
                    SubRegion = new SubRegion(),
                    Population = restCountry.population,
                    GPSLocation = (restCountry.latlng != null) ? new GPSLocation() { Latitude = restCountry.latlng.Latitude, Longitude = restCountry.latlng.Longitude } : null,
                    Demonym = restCountry.demonym,
                    Area = restCountry.area,
                    Gini = restCountry.gini,
                    TimeZones = new List<CustomTimeZone>(),
                    Borders = new List<Country>(),
                    Currencies = new List<Currency>(),
                    Languages = new List<Language>(),
                    Translations = (restCountry.translations != null) ? new Translation()
                    {
                        de = restCountry.translations.de,
                        es = restCountry.translations.es,
                        fr = restCountry.translations.fr,
                        ja = restCountry.translations.ja,
                        it = restCountry.translations.it,
                        br = restCountry.translations.br,
                        pt = restCountry.translations.pt,
                        nl = restCountry.translations.nl,
                        hr = restCountry.translations.hr,
                        fa = restCountry.translations.fa,
                    } : null,
                    Flag = restCountry.flag.Source,
                    RegionalBlocs = new List<RegionalBloc>(),
                    CIOC = restCountry.cioc,
                    NativeName = restCountry.nativeName,
                    NumericCode = restCountry.numericCode,

                };

                List<Task> TaskController = new List<Task>()
                {
                    Task.Run( () =>  LoadCountyrTopLevelDomains(newCountry,restCountry.topLevelDomain) ),
                    Task.Run( () =>  LoadCountryCallingCodes(newCountry,restCountry.callingCodes) ),
                    Task.Run( () =>  LoadCountryAltSpellings(newCountry,restCountry.altSpellings) ),
                    Task.Run( () =>  LoadCountryRegion(newCountry,restCountry.region) ),
                    Task.Run( () =>  LaodCountrySubRegion(newCountry,restCountry.subregion) ),
                    Task.Run( () =>  LoadCountryTimeZones(newCountry,restCountry.timezones) ),
                    Task.Run( () =>  LoadCountryCurrencies(newCountry,restCountry.currencies) ),
                    Task.Run( () =>  LoadCountryLanguages(newCountry,restCountry.languages) ),
                    Task.Run( () =>  LoadCountryRegionalBlocs(newCountry,restCountry.regionalBlocs) ),
                };

                await Task.WhenAll(TaskController);

                WorldController.Countries.Add(newCountry);

                countryReport.Current++;
                countryReport.Message = $"{newCountry.Name} foi carregado [{countryReport.Current}/{countryReport.Goal}] \n";
                countryReport.Percentage = 0.1;
                progress.Report(countryReport);

            }

        }

        /// <summary>
        /// Starts to obtain all own country model Flags
        /// </summary>
        /// <param name="progress">Receives a ReportModel</param>
        /// <returns></returns>
        internal async Task DownloadFlags(IProgress<ReportModel> progress)
        {
            Stopwatch sp = new Stopwatch();
            sp.Start();
            progress.Report(new ReportModel() {Message = "A fazer download das bandeiras em falta \n"});


            List<Task> TaskController = new List<Task>();

            foreach(Country country in WorldController.Countries)
            {
                string countryName = country.Flag.Split('/').ElementAt(4).ToUpper();
                TaskController.Add(Task.Run( () => country.Flag = GetFlagPath(countryName, country.Flag) ) );
            }

            await Task.WhenAll(TaskController);

            sp.Stop();
            progress.Report(new ReportModel() { Message = $"Todas as bandeiras foram descarregadas ({sp.Elapsed.Seconds} segundos) \n" });
        }

        /// <summary>
        /// If Flag doesn't exist localy, Download and sets local path for Country Flag
        /// </summary>
        /// <param name="country">Country Name, this will set the file name</param>
        /// <param name="flagSource">Country Flag Source</param>
        /// <returns>Return the local flag path (if able)</returns>
        private string GetFlagPath(string country, string flagSource)
        {
            if (!Directory.Exists(@"Data\CountriesFlags"))
            {
                Directory.CreateDirectory(@"Data\CountriesFlags");
            }

            string directory = @".\Data\CountriesFlags\";

            string countryFile = $"{directory}{country}";

            if (!File.Exists(countryFile))
            {

                try
                {
                    using (WebClient client = new WebClient())
                    {
                        client.DownloadFile(flagSource, countryFile);
                    }

                    return countryFile;
                }
                catch
                {
                    return null;
                }
            }

            return countryFile;

        }

        /// <summary>
        /// Add all Top Level Domains to creating country
        /// </summary>
        /// <param name="newCountry">Country that is being created</param>
        /// <param name="topLevelDomains">All Top Level Domains from restcountry working with</param>
        private void LoadCountyrTopLevelDomains(Country newCountry, List<RESTTopLevelDomain> topLevelDomains)
        {
            foreach (RESTTopLevelDomain topLevelDomain in topLevelDomains)
            {
                newCountry.TopLevelDomains.Add(topLevelDomain.TopLevelDomain);
            }
        }

        /// <summary>
        /// Add all Calling Codes to creating country
        /// </summary>
        /// <param name="newCountry">Country that is breing created</param>
        /// <param name="callingCodes">All Calling codes from restcountry working it</param>
        private void LoadCountryCallingCodes(Country newCountry, List<RESTCallingCode> callingCodes)
        {
            foreach (RESTCallingCode callingCode in callingCodes)
            {
                newCountry.CallingCodes.Add(callingCode.CallingCode);
            }
        }

        /// <summary>
        /// Add all AltSpellings to creating country
        /// </summary>
        /// <param name="newCountry">Country that is breing created</param>
        /// <param name="altSpellings">All Alt Spelling from restcountry working it</param>
        private void LoadCountryAltSpellings(Country newCountry, List<RESTAltSpelling> altSpellings)
        {
            foreach (RESTAltSpelling altSpelling in altSpellings)
            {
                newCountry.AltSpellings.Add(altSpelling.AltSpelling);
            }
        }

        /// <summary>
        /// Add REgion to creating country
        /// </summary>
        /// <param name="newCountry">Country that is being created</param>
        /// <param name="region">Region from restcountry working it</param>
        private void LoadCountryRegion(Country newCountry, string region)
        {
            foreach (Region worldRegion in WorldController.Regions)
            {
                if (worldRegion.Name.ToUpper() == region.ToUpper())
                {
                    newCountry.Region = worldRegion;

                    worldRegion.Countries.Add(newCountry);
                }
            }
        }

        /// <summary>
        /// Add SubRegion to creating country
        /// </summary>
        /// <param name="newCountry">Country that is being created</param>
        /// <param name="subregion">Subregion from restcountry working it</param>
        private void LaodCountrySubRegion(Country newCountry, string subregion)
        {
            foreach (SubRegion worldSubRegion in WorldController.SubRegions)
            {
                if (worldSubRegion.Name.ToUpper() == subregion.ToUpper())
                {
                    newCountry.SubRegion = worldSubRegion;

                    worldSubRegion.Countries.Add(newCountry);
                }
            }
        }

        /// <summary>
        /// Add All Timezone to creating country
        /// </summary>
        /// <param name="newCountry">Country that is being created</param>
        /// <param name="timezones">All Timezones from restcountry working it</param>
        private void LoadCountryTimeZones(Country newCountry, List<RESTTimeZone> timezones)
        {
            foreach (RESTTimeZone timeZone in timezones)
            {
                foreach (CustomTimeZone customTimeZone in WorldController.CustomTimeZones)
                {

                    if (timeZone.ToString().ToUpper() == customTimeZone.ToString().ToUpper())
                    {
                        newCountry.TimeZones.Add(customTimeZone);

                        customTimeZone.Countries.Add(newCountry);
                    }
                }
            }
        }

        /// <summary>
        /// Add all Currencies to creating country
        /// </summary>
        /// <param name="newCountry">Country that is being created</param>
        /// <param name="currencies">All currencies from restcountry working it</param>
        private void LoadCountryCurrencies(Country newCountry, List<RESTCurrency> currencies)
        {
            foreach (RESTCurrency restCurrency in currencies)
            {
                foreach (Currency currency in WorldController.Currencies)
                {

                    string restCurrencyName = restCurrency.name;
                    string currencyName = currency.Name;


                    if (restCurrency.name == null)
                    {
                        restCurrencyName = "null";
                    }
                    if (currency.Name == null)
                    {
                        currencyName = "null";
                    }

                    if (restCurrencyName.ToUpper() == currencyName.ToUpper())
                    {
                        newCountry.Currencies.Add(currency);

                        currency.Countries.Add(newCountry);
                    }
                }
            }
        }

        /// <summary>
        /// All all Languages to creating country
        /// </summary>
        /// <param name="newCountry">Country that is being created</param>
        /// <param name="languages">All languages from restcountry working it</param>
        private void LoadCountryLanguages(Country newCountry, List<RESTLanguage> languages)
        {
            foreach (RESTLanguage rESTLanguage in languages)
            {
                foreach (Language language in WorldController.Languages)
                {

                    if (rESTLanguage.name.ToUpper() == language.Name.ToUpper())
                    {
                        newCountry.Languages.Add(language);

                        language.Countries.Add(newCountry);
                    }
                }
            }
        }

        /// <summary>
        /// Add all Regional Bloc to creating country
        /// </summary>
        /// <param name="newCountry">Country that is being created</param>
        /// <param name="regionalBlocs">All regionalblocs from restcountry working it</param>
        private void LoadCountryRegionalBlocs(Country newCountry, List<RESTRegionalBloc> regionalBlocs)
        {
            foreach (RESTRegionalBloc restRegionalBloc in regionalBlocs)
            {
                foreach (RegionalBloc regionalBloc in WorldController.RegionalBlocs)
                {
                    if (restRegionalBloc.name.ToUpper() == regionalBloc.Name.ToUpper())
                    {
                        newCountry.RegionalBlocs.Add(regionalBloc);

                        regionalBloc.Countries.Add(newCountry);
                    }
                }
            }
        }

        /// <summary>
        /// Add all Borders (Country Models) to each country
        /// </summary>
        /// <param name="restCountries">Lsit of all countries from API</param>
        internal void LoadCountryBorders(List<RESTCountry> restCountries)
        {
            foreach (Country country in WorldController.Countries)
            {
                foreach (RESTCountry restCountry in restCountries)
                {
                    if (country.Alpha3Code.ToUpper() == restCountry.alpha3Code.ToUpper())
                    {
                        foreach (RESTBorder border in restCountry.borders)
                        {
                            foreach (Country countryBorder in WorldController.Countries)
                            {
                                if (countryBorder.Alpha3Code.ToUpper() == border.Border.ToUpper())
                                {
                                    country.Borders.Add(countryBorder);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
