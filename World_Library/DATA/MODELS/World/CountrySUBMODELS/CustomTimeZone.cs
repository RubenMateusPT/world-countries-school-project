﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace World_Library.DATA.MODELS.World.CountrySUBMODELS
{
    /// <summary>
    /// Own Custom TimeZone Model
    /// </summary>
    public class CustomTimeZone
    {
        /// <summary>
        /// Time Zone Code
        /// </summary>
        public string TimeZoneCode { get; set; }

        /// <summary>
        /// Time Zone Negative/Positive
        /// </summary>
        public bool Positive { get; set; }

        /// <summary>
        /// Time Zone OffSet in Miliseconds
        /// </summary>
        public double MilisecondsOffSet { get; set; }

        /// <summary>
        /// Countries that habe this timezone
        /// </summary>
        public List<Country> Countries { get; set; } = new List<Country>();

        /// <summary>
        /// Sends Timezone in this format: "UTC+01.00"
        /// </summary>
        public string Name
        {
            get
            {
                return ToString();
            }
        }

        /// <summary>
        /// Creates Own TimeZone ToString()
        /// </summary>
        /// <returns>Own TimeZone string format</returns>
        public override string ToString()
        {
            if (Positive)
            {
                return $"{TimeZoneCode}+{TimeSpan.FromMilliseconds(MilisecondsOffSet)}";
            }
            return $"{TimeZoneCode}-{TimeSpan.FromMilliseconds(MilisecondsOffSet)}";
        }
    }
}
