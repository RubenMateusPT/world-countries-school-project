﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace World_Library.DATA.MODELS.World.CountrySUBMODELS
{
    /// <summary>
    /// Own Region Model
    /// </summary>
    public class Region
    {
        /// <summary>
        /// Region Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// SubRegions that have this Region
        /// </summary>
        public List<SubRegion> SubRegions { get; set; } = new List<SubRegion>();

        /// <summary>
        /// Countries that have this Region
        /// </summary>
        public List<Country> Countries { get; set; } = new List<Country>();

        /// <summary>
        /// Return Name of Region
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{Name}";
        }
    }
}
