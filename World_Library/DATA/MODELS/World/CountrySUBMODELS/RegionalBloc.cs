﻿using System.Collections.Generic;

namespace World_Library.DATA.MODELS.World.CountrySUBMODELS
{
    /// <summary>
    /// Own RegionBloc Model
    /// </summary>
    public class RegionalBloc
    {
        /// <summary>
        /// Regional Bloc Acronm
        /// </summary>
        public string Acronym { get; set; }

        /// <summary>
        /// Regional Bloc Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Regional Blos OtherAcronyms
        /// </summary>
        public  List<string> OtherAcronyms { get; set; }

        /// <summary>
        /// Regional Bloc OtherNames
        /// </summary>
        public  List<string> OtherNames{ get; set; }

        /// <summary>
        /// Countries that have this RegionalBloc
        /// </summary>
        public List<Country> Countries { get; set; } = new List<Country>();

    }
}
