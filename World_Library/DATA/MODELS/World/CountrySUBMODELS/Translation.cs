﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace World_Library.DATA.MODELS.World.CountrySUBMODELS
{
    /// <summary>
    /// Own Translation Model
    /// </summary>
    public class Translation
    {
        /// <summary>
        /// Translation in German
        /// </summary>
        public string de { get; set; }

        /// <summary>
        /// Translation in Spanish
        /// </summary>
        public string es { get; set; }

        /// <summary>
        /// Translation in French
        /// </summary>
        public string fr { get; set; }

        /// <summary>
        /// Translation in Japanese
        /// </summary>
        public string ja { get; set; }

        /// <summary>
        /// Translation in Italian
        /// </summary>
        public string it { get; set; }

        /// <summary>
        /// Translation in Portuguese(Brazil)
        /// </summary>
        public string br { get; set; }

        /// <summary>
        /// Translation in Portuguese
        /// </summary>
        public string pt { get; set; }

        /// <summary>
        /// Translation in Netherland
        /// </summary>
        public string nl { get; set; }

        /// <summary>
        /// Translation in Croatian
        /// </summary>
        public string hr { get; set; }

        /// <summary>
        /// Translation in Persian
        /// </summary>
        public string fa { get; set; }
    }
}
