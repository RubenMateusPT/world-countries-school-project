﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace World_Library.DATA.MODELS.World.CountrySUBMODELS
{
    /// <summary>
    /// Own GpsLocation Model
    /// </summary>
    public class GPSLocation
    {
        /// <summary>
        /// GPS Latitude
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// GPS Longitude
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// Sends GPS Model int this format: "Lat: 50º | Long: 25º"
        /// </summary>
        public string GPS
        {
            get { return $"Lat: {Latitude}º | Long: {Longitude}º"; }
        }
    }
}
