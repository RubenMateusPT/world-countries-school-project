﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace World_Library.DATA.MODELS.World.CountrySUBMODELS
{
    /// <summary>
    /// Own SubRegion Model
    /// </summary>
    public class SubRegion
    {
        /// <summary>
        /// SubRegion Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Countries that have this SubREgion
        /// </summary>
        public List<Country> Countries { get; set; } = new List<Country>();

        /// <summary>
        /// This SubRegion Region
        /// </summary>
        public Region Region { get; set; }

    }
}
