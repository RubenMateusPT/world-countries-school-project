﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace World_Library.DATA.MODELS.World.CountrySUBMODELS
{
    /// <summary>
    /// Own Language Model
    /// </summary>
    public class Language
    {
        /// <summary>
        /// Language Iso639_1
        /// </summary>
        public string Iso639_1 { get; set; }

        /// <summary>
        /// Language Iso639_2
        /// </summary>
        public string Iso639_2 { get; set; }

        /// <summary>
        /// Language Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Language Native Name
        /// </summary>
        public string NativeName { get; set; }

        /// <summary>
        /// Countries that habe this Language
        /// </summary>
        public List<Country> Countries { get; set; } = new List<Country>();

    }
}
