﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace World_Library.DATA.MODELS.World.CountrySUBMODELS
{
    /// <summary>
    /// Own Currency Model
    /// </summary>
    public class Currency
    {
        /// <summary>
        /// Currency Code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Currency Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Currency Symbol
        /// </summary>
        public string Symbol { get; set; }

        /// <summary>
        /// Currency TaxRAte
        /// </summary>
        public double? TaxRate { get; set; }

        /// <summary>
        /// Countries that have this Currency
        /// </summary>
        public List<Country> Countries { get; set; } = new List<Country>();
    }
}
