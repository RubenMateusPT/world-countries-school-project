﻿using System.Collections.Generic;
using World_Library.DATA.MODELS.World.CountrySUBMODELS;

namespace World_Library.DATA.MODELS
{
    /// <summary>
    /// Own Country Model
    /// </summary>
    public class Country
    {
        /// <summary>
        /// Country Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Country Top Level Domains
        /// </summary>
        public List<string> TopLevelDomains { get; set; }

        /// <summary>
        /// Country Alpha 2 Code
        /// </summary>
        public string Alpha2Code { get; set; }

        /// <summary>
        /// Country Aplha 3 Code
        /// </summary>
        public string Alpha3Code { get; set; }

        /// <summary>
        /// Country Calling Codes
        /// </summary>
        public List<string> CallingCodes { get; set; }

        /// <summary>
        /// Country Capital
        /// </summary>
        public string Capital { get; set; }

        /// <summary>
        /// Country Alt Spellings
        /// </summary>
        public List<string> AltSpellings { get; set; }

        /// <summary>
        /// Country Region Model
        /// </summary>
        public  Region Region { get; set; }

        /// <summary>
        /// Country SubRegion Model
        /// </summary>
        public  SubRegion SubRegion { get; set; }

        /// <summary>
        /// Country Population
        /// </summary>
        public int Population { get; set; }

        /// <summary>
        /// Country GPS Location Model
        /// </summary>
        public  GPSLocation GPSLocation { get; set; }

        /// <summary>
        /// Country Demonym
        /// </summary>
        public string Demonym { get; set; }

        /// <summary>
        /// Country Area (May be null) 
        /// </summary>
        public double? Area { get; set; }

        /// <summary>
        /// Country Gini (May be null)
        /// </summary>
        public double? Gini { get; set; }

        /// <summary>
        /// Country TimeZones Models
        /// </summary>
        public  List<CustomTimeZone> TimeZones { get; set; }

        /// <summary>
        /// Country Borders (Country Model)
        /// </summary>
        public List<Country> Borders { get; set; }

        /// <summary>
        /// Country Native Name
        /// </summary>
        public string NativeName { get; set; }

        /// <summary>
        /// Country NumericCode
        /// </summary>
        public string NumericCode { get; set; }

        /// <summary>
        /// Country Currencies (Currency Models)
        /// </summary>
        public  List<Currency> Currencies { get; set; }

        /// <summary>
        /// Country Languages (Language Models)
        /// </summary>
        public  List<Language> Languages { get; set; }

        /// <summary>
        /// Country Translations Model
        /// </summary>
        public  Translation Translations { get; set; }

        /// <summary>
        /// Country Flag Local Path
        /// </summary>
        public string Flag { get; set; }

        /// <summary>
        /// County RegionalBlocs (REgionalBloc Models)
        /// </summary>
        public List<RegionalBloc> RegionalBlocs { get; set; }

        /// <summary>
        /// Country CIOC
        /// </summary>
        public string CIOC { get; set; }
    }
}
