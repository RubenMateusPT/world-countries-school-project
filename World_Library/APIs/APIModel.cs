﻿namespace World_Library.APIs
{
    /// <summary>
    /// APIModel
    /// </summary>
    public class APIModel
    {
        /// <summary>
        /// APIModel API Name
        /// </summary>
        public string ApiName { get; set; }

        /// <summary>
        /// APIModel API Base Address
        /// </summary>
        public string Controller { get; set; }

        /// <summary>
        /// APIModel API Action Address
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// APIModel Constructor
        /// </summary>
        /// <param name="ApiName">Defines the API Name</param>
        /// <param name="Controller">Defines the base address of API </param>
        /// <param name="Action">Defines the action to be done on API Base Address</param>
        public APIModel(string ApiName, string Controller, string Action)
        {
            this.ApiName = ApiName;
            this.Controller = Controller;
            this.Action = Action;
        }
    }
}
