﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace World_Library.APIs.RestCountriesModel.SubModels
{
    /// <summary>
    /// Rest Country Translations Model
    /// </summary>
    internal class RESTTranslations
    {
        /// <summary>
        /// Rest Country Translations Unique ID
        /// </summary>
        [JsonIgnore]
        [Key]
        public int ID { get; set; }

        /// <summary>
        /// Rest Country Translation in German
        /// </summary>
        [JsonProperty(PropertyName = "de")]
        public string de { get; set; }

        /// <summary>
        /// Rest Country Translation in Spanish
        /// </summary>
        [JsonProperty(PropertyName = "es")]
        public string es { get; set; }

        /// <summary>
        /// Rest Country Translation in French
        /// </summary>
        [JsonProperty(PropertyName = "fr")]
        public string fr { get; set; }

        /// <summary>
        /// Rest Country Translation in Japanese
        /// </summary>
        [JsonProperty(PropertyName = "ja")]
        public string ja { get; set; }

        /// <summary>
        /// Rest Country Translation in Italian
        /// </summary>
        [JsonProperty(PropertyName = "it")]
        public string it { get; set; }

        /// <summary>
        /// Rest Country Translation in Portuguese(Brasil)
        /// </summary>
        [JsonProperty(PropertyName = "br")]
        public string br { get; set; }

        /// <summary>
        /// Rest Country Translation in Portuguese
        /// </summary>
        [JsonProperty(PropertyName = "pt")]
        public string pt { get; set; }

        /// <summary>
        /// Rest Country Translation in Netherlands
        /// </summary>
        [JsonProperty(PropertyName = "nl")]
        public string nl { get; set; }

        /// <summary>
        /// Rest Country Translation in Croatian
        /// </summary>
        [JsonProperty(PropertyName = "hr")]
        public string hr { get; set; }

        /// <summary>
        /// Rest Country Translation in Persian
        /// </summary>
        [JsonProperty(PropertyName = "fa")]
        public string fa { get; set; }

    }
}
