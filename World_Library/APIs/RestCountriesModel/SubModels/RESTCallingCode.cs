﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace World_Library.APIs.RestCountriesModel.SubModels
{
    /// <summary>
    /// Rest Country Calling Code Model
    /// </summary>
    internal class RESTCallingCode
    {
        /// <summary>
        /// Rest Country Calling Code Unique ID
        /// </summary>
        [JsonIgnore]
        [Key]
        public int ID { get; set; }

        /// <summary>
        /// Name of Rest Country Calling Code
        /// </summary>
        public string CallingCode { get; set; }

    }
}
