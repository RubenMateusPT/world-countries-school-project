﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace World_Library.APIs.RestCountriesModel.SubModels
{
    /// <summary>
    /// Rest Country Language Model
    /// </summary>
    internal class RESTLanguage
    {
        /// <summary>
        /// Rest Country Language Unique ID
        /// </summary>
        [JsonIgnore]
        [Key]
        public int ID { get; set; }

        /// <summary>
        /// Rest Country Language Iso639_1
        /// </summary>
        [JsonProperty(PropertyName = "iso639_1")]
        public string iso639_1 { get; set; }

        /// <summary>
        /// Rest Country Language Iso639_2
        /// </summary>
        [JsonProperty(PropertyName = "iso639_2")]
        public string iso639_2 { get; set; }

        /// <summary>
        /// Rest Country Language Name
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }

        /// <summary>
        /// Rest Country Language Native Name
        /// </summary>
        [JsonProperty(PropertyName = "nativeName")]
        public string nativeName { get; set; }

    }
}
