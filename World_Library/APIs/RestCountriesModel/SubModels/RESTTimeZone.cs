﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace World_Library.APIs.RestCountriesModel.SubModels
{
    /// <summary>
    /// Rest Country Custom TimeZone Model
    /// </summary>
    internal class RESTTimeZone
    {
        /// <summary>
        /// Rest Country Custom TimeZone Unique ID
        /// </summary>
        [JsonIgnore]
        [Key]
        public int ID { get; set; }

        /// <summary>
        /// Rest Country Custom TimeZone Code
        /// </summary>
        public string TimeZoneCode { get; set; }

        /// <summary>
        /// Rest Country Custom TimeZone Negative/Positive Zone
        /// </summary>
        public bool Positive { get; set; }

        /// <summary>
        /// Rest Country Custom TimeZone Time Offset in Miliseconds
        /// </summary>
        public double MilisecondsOffSet { get; set; }


        public override string ToString()
        {
            if (Positive)
            {
                return $"{TimeZoneCode}+{TimeSpan.FromMilliseconds(MilisecondsOffSet)}";
            }
            return $"{TimeZoneCode}-{TimeSpan.FromMilliseconds(MilisecondsOffSet)}";
        }
    }
}
