﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace World_Library.APIs.RestCountriesModel.SubModels
{
    /// <summary>
    /// Rest Country Border Model
    /// </summary>
    internal class RESTBorder
    {
        /// <summary>
        /// Rest Country Border Unique ID
        /// </summary>
        [JsonIgnore]
        [Key]
        public int ID { get; set; }

        /// <summary>
        /// Name of Country of Rest Coountry Border
        /// </summary>
        public string Border { get; set; }

    }
}
