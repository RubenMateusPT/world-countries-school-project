﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace World_Library.APIs.RestCountriesModel.SubModels
{
    /// <summary>
    /// Rest Country Top Level Domain Model
    /// </summary>
    internal class RESTTopLevelDomain
    {
        /// <summary>
        /// Rest Country Top Level Domain Unique ID
        /// </summary>
        [JsonIgnore] [Key]
        public int ID { get; set; }

        /// <summary>
        /// Rest Country Top Level Domain Name
        /// </summary>
        public string TopLevelDomain { get; set; }

    }
}
