﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace World_Library.APIs.RestCountriesModel.SubModels
{
    /// <summary>
    /// Rest Country Flag Model
    /// </summary>
    internal class RESTFlag
    {
        /// <summary>
        /// Rest Country Flag Unique ID
        /// </summary>
        [JsonIgnore]
        [Key]
        public int ID { get; set; }

        /// <summary>
        /// Rest Country Flag Source
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// Rest Country Flag Local File Path
        /// </summary>
        public string Flag { get; set; }

    }
}
