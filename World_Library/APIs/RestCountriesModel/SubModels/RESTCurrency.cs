﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace World_Library.APIs.RestCountriesModel.SubModels
{
    /// <summary>
    /// Rest Country Currency Model
    /// </summary>
    internal class RESTCurrency
    {
        /// <summary>
        /// Rest Country Currency Unique ID
        /// </summary>
        [JsonIgnore]
        [Key]
        public int ID { get; set; }

        /// <summary>
        /// Rest Country Currency Code
        /// </summary>
        [JsonProperty(PropertyName = "code")]
        public string code { get; set; }

        /// <summary>
        /// Rest Country Currency Name
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }

        /// <summary>
        /// Rest Country Currency Symbol
        /// </summary>
        [JsonProperty(PropertyName = "symbol")]
        public string symbol { get; set; }

    }
}
