﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using World_Library.APIs.RestCountriesModel.Deserializer;
using World_Library.APIs.RestCountriesModel.SubModels.RegionalBlocSubModels;

namespace World_Library.APIs.RestCountriesModel.SubModels
{
    /// <summary>
    /// Rest Country Regional Bloc Model
    /// </summary>
    internal class RESTRegionalBloc
    {
        /// <summary>
        /// Rest Country Regional Bloc Unique ID
        /// </summary>
        [JsonIgnore]
        [Key]
        public int ID { get; set; }

        /// <summary>
        /// Rest Country Regional Bloc Acronym
        /// </summary>
        [JsonProperty(PropertyName = "acronym")]
        public string acronym { get; set; }

        /// <summary>
        /// Rest Country Regional Bloc Name
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }

        /// <summary>
        /// Rest Country Regional Bloc OtherAcronyms
        /// </summary>
        [JsonProperty(PropertyName = "otherAcronyms", ItemConverterType = typeof(RESTRegionalBlocOtherAcronymDeserializer))]
        public  List<RESTRegionalBlocOtherAcronym> otherAcronyms { get; set; }

        /// <summary>
        /// Rest Country Regional Bloc OtherAcronyms
        /// </summary>
        [JsonProperty(PropertyName = "otherNames", ItemConverterType = typeof(RESTRegionalBlocOtherNameDeserializer))]
        public  List<RESTRegionalBlocOtherName> otherNames { get; set; }

    }
}
