﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace World_Library.APIs.RestCountriesModel.SubModels
{
    /// <summary>
    /// Rest Country AltSpelling Model
    /// </summary>
    internal class RESTAltSpelling
    {
        /// <summary>
        /// Rest Country AltSpelling Unique ID
        /// </summary>
        [JsonIgnore]
        [Key]
        public int ID { get; set; }


        /// <summary>
        /// Name of Rest Country AltSpelling
        /// </summary>
        public string AltSpelling { get; set; }

    }
}
