﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace World_Library.APIs.RestCountriesModel.SubModels
{
    /// <summary>
    /// Rest Country GPS Location Model
    /// </summary>
    internal class RESTGPSLocation
    {
        /// <summary>
        /// Rest Country GPS Location Unique ID
        /// </summary>
        [JsonIgnore]
        [Key]
        public int ID { get; set; }

        /// <summary>
        /// Rest Country GPS Location Latitude
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Rest Country GPS Location Longitude
        /// </summary>
        public double Longitude { get; set; }

    }
}
