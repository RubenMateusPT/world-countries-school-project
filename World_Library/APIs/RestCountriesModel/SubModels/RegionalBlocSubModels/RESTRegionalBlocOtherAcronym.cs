﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace World_Library.APIs.RestCountriesModel.SubModels.RegionalBlocSubModels
{
    /// <summary>
    /// REST RegionalBloc OtherAcronym Api Model
    /// </summary>
    internal class RESTRegionalBlocOtherAcronym
    {
        /// <summary>
        /// Rest RegionalBloc OtherAcronym Unique ID
        /// </summary>
        [JsonIgnore]
        [Key]
        public int ID { get; set; }

        /// <summary>
        /// Name of the Rest RegionalBloc OtherAcronym
        /// </summary>
        public string OTherAcronym { get; set; }

    }
}
