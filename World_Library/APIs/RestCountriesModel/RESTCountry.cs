﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using World_Library.APIs.RestCountriesModel.Deserializer;
using World_Library.APIs.RestCountriesModel.SubModels;

namespace World_Library.APIs.RestCountriesModel
{
    /// <summary>
    /// Rest Country Model
    /// </summary>
    internal class RESTCountry
    {
        /// <summary>
        /// Rest Country Unique ID
        /// </summary>
        [JsonIgnore]
        [Key]
        public int RESTCountryID { get; set; }

        /// <summary>
        /// Rest Country Name
        /// </summary>
        [JsonProperty(PropertyName ="name")]
        public string name { get; set; }

        /// <summary>
        /// Rest Country TopLevelDomains Models
        /// </summary>
        [JsonProperty(PropertyName = "topLevelDomain", ItemConverterType = typeof(RESTCountryTopLevelDomainDeserializer))]
        public  List<RESTTopLevelDomain> topLevelDomain { get; set; }

        /// <summary>
        /// Rest Country Alpha 2 Code
        /// </summary>
        [JsonProperty(PropertyName = "alpha2Code")]
        public string alpha2Code { get; set; }

        /// <summary>
        /// Rest Country Alpha 3 Code
        /// </summary>
        [JsonProperty(PropertyName = "alpha3Code")]
        public string alpha3Code { get; set; }

        /// <summary>
        /// Rest Country Calling Codes Models
        /// </summary>
        [JsonProperty(PropertyName = "callingCodes", ItemConverterType = typeof(RESTCountryCallingCodeDeserializer))]
        public  List<RESTCallingCode> callingCodes { get; set; }

        /// <summary>
        /// Rest Country Capital
        /// </summary>
        [JsonProperty(PropertyName = "capital")]
        public string capital { get; set; }

        /// <summary>
        /// Rest Country AltSpellings Models
        /// </summary>
        [JsonProperty(PropertyName = "altSpellings", ItemConverterType = typeof(RESTCountryAltSpellingDeserializer))]
        public  List<RESTAltSpelling> altSpellings { get; set; }

        /// <summary>
        /// Rest Country Region
        /// </summary>
        [JsonProperty(PropertyName = "region")]
        public string region { get; set; }

        /// <summary>
        /// Rest Country SubRegion
        /// </summary>
        [JsonProperty(PropertyName = "subregion")]
        public string subregion { get; set; }

        /// <summary>
        /// Rest Country Population
        /// </summary>
        [JsonProperty(PropertyName = "population")]
        public int population { get; set; }

        /// <summary>
        /// Rest Country GPS Location Model
        /// </summary>
        [JsonProperty(PropertyName = "latlng")]
        [JsonConverter(typeof(RESTCountryGPSLocationDeserializer))]
        public  RESTGPSLocation latlng { get; set; }

        /// <summary>
        /// Rest Country Demonym
        /// </summary>
        [JsonProperty(PropertyName = "demonym")]
        public string demonym { get; set; }

        /// <summary>
        /// Rest Country Area
        /// </summary>
        [JsonProperty(PropertyName = "area")]
        public double? area { get; set; }

        /// <summary>
        /// Rest Country Gini
        /// </summary>
        [JsonProperty(PropertyName = "gini")]
        public double? gini { get; set; }

        /// <summary>
        /// Rest Country TimeZones Model
        /// </summary>
        [JsonProperty(PropertyName = "timezones", ItemConverterType = typeof(RESTCountryTimeZoneDeserializer))]
        public  List<RESTTimeZone> timezones { get; set; }

        /// <summary>
        /// Rest Country Borders Models
        /// </summary>
        [JsonProperty(PropertyName = "borders", ItemConverterType = typeof(RESTCountryBorderDeserializer))]
        public  List<RESTBorder> borders { get; set; }

        /// <summary>
        /// Rest Country Native Name
        /// </summary>
        [JsonProperty(PropertyName = "nativeName")]
        public string nativeName { get; set; }

        /// <summary>
        /// Rest Country Numeric Code
        /// </summary>
        [JsonProperty(PropertyName = "numericCode")]
        public string numericCode { get; set; }

        /// <summary>
        /// Rest Country Currencies Models
        /// </summary>
        [JsonProperty(PropertyName = "currencies")]
        public  List<RESTCurrency> currencies { get; set; }

        /// <summary>
        /// Rest Country Languages Models
        /// </summary>
        [JsonProperty(PropertyName = "languages")]
        public  List<RESTLanguage> languages { get; set; }

        /// <summary>
        /// Rest Country Translations Model
        /// </summary>
        [JsonProperty(PropertyName = "translations")]
        public  RESTTranslations translations { get; set; }

        /// <summary>
        /// Rest Country Flag Model
        /// </summary>
        [JsonProperty(PropertyName = "flag")]
        [JsonConverter(typeof(RESTCountryFlagDeserializer))]
        public  RESTFlag flag { get; set; }

        /// <summary>
        /// Rest Country RegionalBlocs Models
        /// </summary>
        [JsonProperty(PropertyName = "regionalBlocs")]
        public  List<RESTRegionalBloc> regionalBlocs { get; set; }

        /// <summary>
        /// Rest Country CIOC
        /// </summary>
        [JsonProperty(PropertyName = "cioc")]
        public string cioc { get; set; }
    }
}
