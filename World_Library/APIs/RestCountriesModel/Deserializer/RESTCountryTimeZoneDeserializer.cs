﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library.APIs.RestCountriesModel.SubModels;

namespace World_Library.APIs.RestCountriesModel.Deserializer
{
    /// <summary>
    /// Custom Json Deserializer of RESTCountriesTimeZones
    /// </summary>
    internal class RESTCountryTimeZoneDeserializer : JsonConverter<RESTTimeZone>
    {
        public override void WriteJson(JsonWriter writer, RESTTimeZone value, JsonSerializer serializer)
        {
            writer.WriteValue(value.ToString());
        }

        public override RESTTimeZone ReadJson(JsonReader reader, Type objectType, RESTTimeZone existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string timezone = (string)reader.Value;

            return GetTimezone(timezone);
        }

        /// <summary>
        /// Creates custom timezone model for RestCountries Api Model
        /// </summary>
        /// <param name="timezone">Json timezone string</param>
        /// <returns></returns>
        private RESTTimeZone GetTimezone(string timezone)
        {

            RESTTimeZone newTimezone = new RESTTimeZone();

            if (timezone.Contains('+'))
            {

                newTimezone = new RESTTimeZone()
                {

                    TimeZoneCode = timezone.Split('+').ElementAt(0),
                    MilisecondsOffSet = TimeSpan.Parse(timezone.Split('+').ElementAt(1)).TotalMilliseconds,
                    Positive = true,
                };
            }
            else if (timezone.Contains('-'))
            {
                newTimezone = new RESTTimeZone()
                {
                    TimeZoneCode = timezone.Split('-').ElementAt(0),
                    MilisecondsOffSet = TimeSpan.Parse(timezone.Split('-').ElementAt(1)).TotalMilliseconds,
                    Positive = false,
                };
            }
            else
            {
                newTimezone = new RESTTimeZone()
                {
                    TimeZoneCode = timezone,
                };
            }

            return newTimezone;
        }


    }
}
