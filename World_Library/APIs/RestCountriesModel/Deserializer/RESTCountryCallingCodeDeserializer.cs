﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library.APIs.RestCountriesModel.SubModels;

namespace World_Library.APIs.RestCountriesModel.Deserializer
{
    /// <summary>
    /// Custom Json Deserializer of RESTCountries Calling Codes
    /// </summary>
    internal class RESTCountryCallingCodeDeserializer : JsonConverter<RESTCallingCode>
    {
        public override void WriteJson(JsonWriter writer, RESTCallingCode value, JsonSerializer serializer)
        {
            writer.WriteValue(value);
        }

        public override RESTCallingCode ReadJson(JsonReader reader, Type objectType, RESTCallingCode existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string callingCode = (string)reader.Value;

            return new RESTCallingCode()
            {
                CallingCode = callingCode,
            };
        }
    }
}
