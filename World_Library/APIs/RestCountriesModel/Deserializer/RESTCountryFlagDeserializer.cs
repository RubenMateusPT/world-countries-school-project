﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using World_Library.APIs.RestCountriesModel.SubModels;

namespace World_Library.APIs.RestCountriesModel.Deserializer
{
    /// <summary>
    /// Custom Json Deserializer of RESTCountries Flags
    /// </summary>
    internal class RESTCountryFlagDeserializer : JsonConverter<RESTFlag>
    {
        public override void WriteJson(JsonWriter writer, RESTFlag value, JsonSerializer serializer)
        {
            writer.WriteValue(value);
        }

        public override RESTFlag ReadJson(JsonReader reader, Type objectType, RESTFlag existingValue, bool hasExistingValue, JsonSerializer serializer)
        {

            string flagSource = (string)reader.Value;

            string countryName = flagSource.Split('/').ElementAt(4);

            string file = $@".\Data\CountriesFlags\{countryName.ToUpper()}";

            return new RESTFlag()
            {
                Source = flagSource,
                Flag = file,
            };
        }

    }
}
