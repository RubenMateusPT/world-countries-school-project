﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library.APIs.RestCountriesModel.SubModels;

namespace World_Library.APIs.RestCountriesModel.Deserializer
{
    /// <summary>
    /// Custom Json Deserializer of RESTCountries Borders
    /// </summary>
    internal class RESTCountryBorderDeserializer : JsonConverter<RESTBorder>
    {

        public override void WriteJson(JsonWriter writer, RESTBorder value, JsonSerializer serializer)
        {
            writer.WriteValue(value);
        }


        public override RESTBorder ReadJson(JsonReader reader, Type objectType, RESTBorder existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string border = (string)reader.Value;

            return new RESTBorder()
            {
                Border = border,
            };
        }

        
    }
}
