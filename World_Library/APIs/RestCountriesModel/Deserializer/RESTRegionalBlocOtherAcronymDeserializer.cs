﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library.APIs.RestCountriesModel.SubModels.RegionalBlocSubModels;

namespace World_Library.APIs.RestCountriesModel.Deserializer
{
    /// <summary>
    /// Custom Json Deserializer of RESTCountries RESTRegionalBloc OtherAcronyms
    /// </summary>
    internal class RESTRegionalBlocOtherAcronymDeserializer : JsonConverter<RESTRegionalBlocOtherAcronym>
    {

        public override void WriteJson(JsonWriter writer, RESTRegionalBlocOtherAcronym value, JsonSerializer serializer)
        {
            writer.WriteValue(value);
        }

        public override RESTRegionalBlocOtherAcronym ReadJson(JsonReader reader, Type objectType, RESTRegionalBlocOtherAcronym existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string otheracronym = (string)reader.Value;

            return new RESTRegionalBlocOtherAcronym()
            {
                OTherAcronym = otheracronym,
            };
        }


    }
}
