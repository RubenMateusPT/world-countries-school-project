﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library.APIs.RestCountriesModel.SubModels;

namespace World_Library.APIs.RestCountriesModel.Deserializer
{
    /// <summary>
    /// Custom Json Deserializer of RESTCountries TopLevelDomains
    /// </summary>
    internal class RESTCountryTopLevelDomainDeserializer : JsonConverter<RESTTopLevelDomain>
    {

        public override void WriteJson(JsonWriter writer, RESTTopLevelDomain value, JsonSerializer serializer)
        {
            writer.WriteValue(value);
        }

        public override RESTTopLevelDomain ReadJson(JsonReader reader, Type objectType, RESTTopLevelDomain existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string topLevelDomain = (string)reader.Value;

            return new RESTTopLevelDomain()
            {
                TopLevelDomain = topLevelDomain,
            };
        }

    }
}
