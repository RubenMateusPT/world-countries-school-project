﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library.APIs.RestCountriesModel.SubModels;

namespace World_Library.APIs.RestCountriesModel.Deserializer
{
    /// <summary>
    /// Custom Json Deserializer of RESTCountries GPS Location
    /// </summary>
    internal class RESTCountryGPSLocationDeserializer : JsonConverter<RESTGPSLocation>
    {
        public override void WriteJson(JsonWriter writer, RESTGPSLocation value, JsonSerializer serializer)
        {
            writer.WriteValue(value);
        }

        public override RESTGPSLocation ReadJson(JsonReader reader, Type objectType, RESTGPSLocation existingValue, bool hasExistingValue, JsonSerializer serializer)
        {

            RESTGPSLocation newGPSLocation = new RESTGPSLocation();

           if(reader.TokenType == JsonToken.StartArray)
            {
                JArray item = JArray.Load(reader);

                if (item.Count != 0)
                {
                    newGPSLocation.Latitude = (double)item.First;
                    newGPSLocation.Longitude = (double)item.Last;
                }
            }


            return newGPSLocation;
        }

        
    }
}
