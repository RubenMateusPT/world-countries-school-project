﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library.APIs.RestCountriesModel.SubModels;

namespace World_Library.APIs.RestCountriesModel.Deserializer
{
    /// <summary>
    /// Custom Json Deserializer of RESTCountries Alt Spellings
    /// </summary>
    internal class RESTCountryAltSpellingDeserializer : JsonConverter<RESTAltSpelling>
    {
        public override void WriteJson(JsonWriter writer, RESTAltSpelling value, JsonSerializer serializer)
        {
            writer.WriteValue(value);
        }

        public override RESTAltSpelling ReadJson(JsonReader reader, Type objectType, RESTAltSpelling existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string altSpelling = (string)reader.Value;

            return new RESTAltSpelling()
            {
               AltSpelling = altSpelling,
            };
        }

    }
}
