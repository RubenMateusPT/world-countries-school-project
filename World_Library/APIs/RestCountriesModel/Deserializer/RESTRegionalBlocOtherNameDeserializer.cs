﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library.APIs.RestCountriesModel.SubModels.RegionalBlocSubModels;

namespace World_Library.APIs.RestCountriesModel.Deserializer
{
    /// <summary>
    /// Custom Json Deserializer of RESTCountries RESTRegionalBloc OtherNames
    /// </summary>
    internal class RESTRegionalBlocOtherNameDeserializer : JsonConverter<RESTRegionalBlocOtherName>
    {
        public override void WriteJson(JsonWriter writer, RESTRegionalBlocOtherName value, JsonSerializer serializer)
        {
            writer.WriteValue(value);
        }

        public override RESTRegionalBlocOtherName ReadJson(JsonReader reader, Type objectType, RESTRegionalBlocOtherName existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string othername = (string)reader.Value;

            return new RESTRegionalBlocOtherName()
            {
                OtherName = othername
            };
        }

        
    }
}
