﻿using System.ComponentModel.DataAnnotations;

namespace World_Library.APIs.CurrencyRatesModel
{
    /// <summary>
    /// Api Rate Model
    /// </summary>
    internal class Rate
    {
        /// <summary>
        /// Unique ID of Rate Model
        /// </summary>
        [Key]        
        public int RateId { get; set; }

        /// <summary>
        /// Currency Code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Currency Tax Rate
        /// </summary>
        public double TaxRate { get; set; }

        /// <summary>
        /// Currency Name
        /// </summary>
        public string Name { get; set; }
    }
}
