﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using World_Library.APIs.RestCountriesModel;
using World_Library.DATA.MODELS;
using World_Library.DATA.MODELS.World.CountrySUBMODELS;
using World_Library.DATABASE.DBCONTEXTS;
using World_Library.SERVICES;
using World_Library.SERVICES.MODELS;

namespace World_Library
{
    /// <summary>
    /// World Crontroller (Master Controller)
    /// </summary>
    public class WorldController
    { 
        /// <summary>
        /// List of all Unique REgions
        /// </summary>
        public static List<Region> Regions { get; set; } = new List<Region>();

        /// <summary>
        /// List of all Unique SubRegions
        /// </summary>
        public static List<SubRegion> SubRegions { get; set; } = new List<SubRegion>();

        /// <summary>
        /// List of all Unique Countries
        /// </summary>
        public static List<Country> Countries { get; set; } = new List<Country>();

        /// <summary>
        /// List of all Unique TimeZones
        /// </summary>
        public static List<CustomTimeZone> CustomTimeZones { get; set; } = new List<CustomTimeZone>();

        /// <summary>
        /// List of all Unique Languages
        /// </summary>
        public static List<Language> Languages { get; set; } = new List<Language>();

        /// <summary>
        /// List of all Unique RegionalBlocs
        /// </summary>
        public static List<RegionalBloc> RegionalBlocs { get; set; } = new List<RegionalBloc>();

        /// <summary>
        /// List of all Unique Currencies
        /// </summary>
        public static List<Currency> Currencies { get; set; } = new List<Currency>();

        /// <summary>
        /// Generates all data needed to work in visual
        /// </summary>
        /// <param name="progress">receives a reportModel</param>
        /// <returns>Returns a unsucessful/sucessful</returns>
        public static async Task<ServiceResponseModel> CreateWorld(IProgress<ReportModel> progress)
        {
                ReportModel reportWorld = new ReportModel() { Current = 0, Goal = 1, Percentage = 0};
                reportWorld.Message = $"Inicio da criação do mundo... [{reportWorld.Current}/{reportWorld.Goal}] \n";
                progress.Report(reportWorld);

                Stopwatch sp = new Stopwatch();
                sp.Start();

            DatabaseService databaseService = new DatabaseService();

            NetworkService networkService = new NetworkService();
            ServiceResponseModel networkResponse = networkService.CheckConnection(progress);
            ServiceResponseModel databaseResponse = new ServiceResponseModel() {Success = true };

            if (networkResponse.Success)
            {
                databaseResponse = await databaseService.UpdateDatabase(progress);
            }

            if (!databaseResponse.Success)
            {

                sp.Stop();
                reportWorld.Message = $"ERRO: Ocorreu um erro ao atualizar as bases de dados. \n";
                progress.Report(reportWorld);

                return new ServiceResponseModel()
                {
                    Success = false,
                    Message = "ERROR: An error has occured while updating databases",
                    EllapsedTime = sp.Elapsed,
                };

            }

            using (RestCountryDBContext RCDB = new RestCountryDBContext()) {
                if (RCDB.RESTCountries.Count() != 250)
                {
                        sp.Stop();
                        reportWorld.Message = $"ERRO: A primeira configuração precisa de conexão à internet \n";
                        progress.Report(reportWorld);

                    return new ServiceResponseModel()
                    {
                        Success = false,
                        Message = "Error: First configuration needs network connection! \n",
                        EllapsedTime = sp.Elapsed,
                    };
                }
            }

            databaseResponse = await databaseService.LoadDatabase(progress);

            if (!databaseResponse.Success)
            {
                sp.Stop();
                reportWorld.Message = $"ERRO: O carregamento da basedados falho! \n";
                progress.Report(reportWorld);

                return new ServiceResponseModel()
                {
                    Success = false,
                    Message = "Error: Datbase Loading has failed! \n",
                    EllapsedTime = sp.Elapsed,
                };
            }

            sp.Stop();
            reportWorld.Message = $"A criação do mundo foi completa e bem feita! ({sp.Elapsed.Seconds} segundos) \n";
            reportWorld.Percentage = 25;
            progress.Report(reportWorld);

            return new ServiceResponseModel() {
                Success = true,
                Message = "World Creation has been succefull",           
            };
        }

    }
}
