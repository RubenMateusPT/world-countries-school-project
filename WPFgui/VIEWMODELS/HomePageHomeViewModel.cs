﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFgui.ViewModels
{
    /// <summary>
    /// Home Page  Home View
    /// </summary>
    public class HomePageHomeViewModel : Screen
    {
        /// <summary>
        /// Home View
        /// </summary>
        public HomePageViewModel HomeView { get; set; }

        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="homePageViewModel"></param>
        public HomePageHomeViewModel(HomePageViewModel homePageViewModel)
        {
            HomeView = homePageViewModel;        
        }

        /// <summary>
        /// Opens the AllCountries View
        /// </summary>
        public void AllCountries()
        {
            HomeView.AllCountries();
        }

        /// <summary>
        /// Opens the AllRegions View
        /// </summary>
        public void Regions()
        {
            HomeView.AllRegions();
        }

        /// <summary>
        /// Opens the AllSubRegions View
        /// </summary>
        public void SubRegions()
        {
            HomeView.AllSubRegions();
        }

        /// <summary>
        /// Opens the AllLanguagues View
        /// </summary>
        public void Languages()
        {
            HomeView.AllLanguagues();
        }

        /// <summary>
        /// Opens the AllCurrencies View
        /// </summary>
        public void Currencies()
        {
            HomeView.AllCurrencies();
        }

        /// <summary>
        /// Opens the AllTimeZonesView
        /// </summary>
        public void TimeZones()
        {
            HomeView.AllTimezones();
        }
    }
}
