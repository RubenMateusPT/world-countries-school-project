﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library.DATA.MODELS;
using World_Library.DATA.MODELS.World.CountrySUBMODELS;

namespace WPFgui.ViewModels
{
    /// <summary>
    /// Country Details View
    /// </summary>
    public class CountryDetailsViewModel : Screen
    {
        /// <summary>
        /// Main Menu View
        /// </summary>
        public MainMenuViewModel MainMenuView { get; set; }

        /// <summary>
        /// Country Main Page View
        /// </summary>
        public CountryMainPageViewModel CountryMainPageView { get; set; }

        /// <summary>
        /// Selected Country to work with
        /// </summary>
        public Country Country { get; set; }

        CustomTimeZone _timeZone;
        /// <summary>
        /// Opens the AllTimezones View with selected timezone and country
        /// </summary>
        public CustomTimeZone TimeZone
        {
            get
            {
                return _timeZone;
            }
            set
            {
                _timeZone = (CustomTimeZone)value;

                CountryMainPageView.TimeZone(TimeZone);
            }
        }

        Country _country;
        /// <summary>
        /// Opens the Country View with selected Border(Country)
        /// </summary>
        public Country SelectedCountry
        {
            get
            {
                return _country;
            }
            set
            {
                _country = value;

                CountryMainPageView.Border(SelectedCountry);
            }
        }

        Currency _currency;
        /// <summary>
        /// Opens the AllCurrencies View with selected currency and country
        /// </summary>
        public Currency Currency
        {
            get
            {
                return _currency;
            }
            set
            {
                _currency = value;

                CountryMainPageView.Currency(Currency);
            }
        }

        Language _language;
        /// <summary>
        /// Opens the AllLanguages View with selected language and country
        /// </summary>
        public Language Language
        {
            get
            {
                return _language;
            }
            set
            {
                _language = value;

                CountryMainPageView.Language(Language);
            }
        }

        RegionalBloc _regionalBloc;
        /// <summary>
        /// Opens the AllRegionalBlocs View with selected REgionalBloc and coutnry
        /// </summary>
        public RegionalBloc RegionalBloc
        {
            get
            {
                return _regionalBloc;
            }
            set
            {
                _regionalBloc = value;

                CountryMainPageView.RegionalBloc(RegionalBloc);
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mainMenuView">Main Page View</param>
        /// <param name="country">Selected Country</param>
        /// <param name="countryMainPageViewModel">Selected Country MainPage View</param>
        public CountryDetailsViewModel(MainMenuViewModel mainMenuView, Country country, CountryMainPageViewModel countryMainPageViewModel)
        {
            this.MainMenuView = mainMenuView;
            this.Country = country;
            this.CountryMainPageView = countryMainPageViewModel;

            MainMenuView.AddressViewName = $"{Country.Name}/Details/";

        }

        /// <summary>
        /// OPens the AllREgion View with selected region and country
        /// </summary>
        public void Region()
        {
            CountryMainPageView.Region(Country.Region);
        }

        /// <summary>
        /// Opens the AllSubregions View with selecte subrefion and country
        /// </summary>
        public void SubRegion()
        {
            CountryMainPageView.SubRegion(Country.SubRegion);
        }
    }
}
