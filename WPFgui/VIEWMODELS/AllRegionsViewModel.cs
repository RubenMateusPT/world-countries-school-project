﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library;
using World_Library.DATA.MODELS;
using World_Library.DATA.MODELS.World.CountrySUBMODELS;

namespace WPFgui.ViewModels
{
    /// <summary>
    /// AllRegions View
    /// </summary>
    public class AllRegionsViewModel : Screen
    {
        /// <summary>
        /// Main Menu View
        /// </summary>
        public MainMenuViewModel MainMenuViewModel { get; set; }

        BindableCollection<Region> _regions = new BindableCollection<Region>(WorldController.Regions.OrderBy(region => region.Name));
        /// <summary>
        /// All Unique REgions
        /// </summary>
        public BindableCollection<Region> Regions
        {
            get
            {
                return _regions;
            }
            set
            {
                _regions = value;
                NotifyOfPropertyChange(() => Regions);
            }
        }

        Region _regionSelected;
        /// <summary>
        /// Selected Region
        /// </summary>
        public Region RegionSelected
        {
            get
            {
                return _regionSelected;
            }
            set
            {
                if (value != null)
                {
                    _regionSelected = (Region)value;

                    NotifyOfPropertyChange(() => RegionSelected);

                    Countries = new BindableCollection<Country>(RegionSelected.Countries);
                    CountryFilterCount = $"Total de Paises: {Countries.Count()}";

                    SearchCountry = SearchCountry;
                }
            }
        }

        BindableCollection<Country> _countries;
        /// <summary>
        /// List of all countries of the same region selected
        /// </summary>
        public BindableCollection<Country> Countries
        {
            get
            {
                return _countries;
            }
            set
            {
                _countries = value;
                NotifyOfPropertyChange(() => Countries);
            }
        }

        Country _CountrySelected;
        /// <summary>
        /// Opens the country view of selected country
        /// </summary>
        public Country CountrySelected
        {
            get
            {
                return _CountrySelected;
            }
            set
            {
                if (value != null)
                {
                    _CountrySelected = (Country)value;

                    NotifyOfPropertyChange(() => CountrySelected);

                    MainMenuViewModel.ActivateItem(new CountryMainPageViewModel(MainMenuViewModel, CountrySelected));
                }
            }
        }

        string _countryFilterCount;
        /// <summary>
        /// Counts how many country are in this filter
        /// </summary>
        public string CountryFilterCount
        {
            get
            {
                return _countryFilterCount;
            }
            set
            {
                _countryFilterCount = value;

                NotifyOfPropertyChange(() => CountryFilterCount);
            }
        }

        string _searchCountry = "";
        /// <summary>
        /// Updates the Countries list based on search country
        /// </summary>
        public string SearchCountry
        {
            get
            {
                return _searchCountry;
            }
            set
            {
                _searchCountry = value;

                NotifyOfPropertyChange(() => value);

                if (RegionSelected != null)
                {
                    Countries = new BindableCollection<Country>(RegionSelected.Countries.Where(country => country.Name.ToLower().StartsWith(value.ToLower())));
                    CountryFilterCount = $"Total de Paises: {Countries.Count()}";
                }

            }
        }

        /// <summary>
        /// Contructor in Main Page
        /// </summary>
        /// <param name="mainMenuViewModel">Main Page View</param>
        public AllRegionsViewModel(MainMenuViewModel mainMenuViewModel)
        {
            this.MainMenuViewModel = mainMenuViewModel;

            MainMenuViewModel.AddressViewName = "Home_Page/Regions";

            
        }

        /// <summary>
        /// Contructor based on Country View
        /// </summary>
        /// <param name="mainMenuViewModel">Main Page View</param>
        /// <param name="CountrySelected">Country from where this call came from</param>
        /// <param name="RegionSelected">Region that was selected</param>
        public AllRegionsViewModel(MainMenuViewModel mainMenuViewModel, Country CountrySelected,Region RegionSelected)
        {
            this.MainMenuViewModel = mainMenuViewModel;

            MainMenuViewModel.AddressViewName = "Home_Page/Regions";

            this.RegionSelected = RegionSelected;
            this.SearchCountry = CountrySelected.Name;
        }
    }
}
