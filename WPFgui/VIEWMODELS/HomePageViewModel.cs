﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace WPFgui.ViewModels
{
    /// <summary>
    /// Home Page View
    /// </summary>
    public class HomePageViewModel : Conductor<object>
    {
        /// <summary>
        /// View of MainMenu
        /// </summary>
        public MainMenuViewModel MainMenuViewModel { get; set; }

        Brush _homeButtonBackground;
        /// <summary>
        /// Background color of the button Home
        /// </summary>
        public Brush HomeButtonBackground
        {
            get
            {
                return _homeButtonBackground;
            }
            set
            {
                _homeButtonBackground = value;

                NotifyOfPropertyChange(() => HomeButtonBackground);
            }
        }        

        Brush _allCountriesButtonBackground;
        /// <summary>
        /// Background color of the button AllCountries
        /// </summary>
        public Brush AllCountriesButtonBackground
        {
            get
            {
                return _allCountriesButtonBackground;
            }
            set
            {
                _allCountriesButtonBackground = value;

                NotifyOfPropertyChange(() => AllCountriesButtonBackground);
            }
        }

        Brush _allRegionsButtonBackground;
        /// <summary>
        /// Background color of the button AllREgion
        /// </summary>
        public Brush AllRegionsButtonBackground
        {
            get
            {
                return _allRegionsButtonBackground;
            }
            set
            {
                _allRegionsButtonBackground = value;

                NotifyOfPropertyChange(() => AllRegionsButtonBackground);
            }
        }

        Brush _allSubregionsButtonBackground;
        /// <summary>
        /// Background color of the button AllSubRegions
        /// </summary>
        public Brush AllSubregionsButtonBackground
        {
            get
            {
                return _allSubregionsButtonBackground;
            }
            set
            {
                _allSubregionsButtonBackground = value;

                NotifyOfPropertyChange(() => AllSubregionsButtonBackground);
            }
        }

        Brush _allLanguagesButtonBackground;
        /// <summary>
        /// Background color of the button AllLanguages
        /// </summary>
        public Brush AllLanguagesButtonBackground
        {
            get
            {
                return _allLanguagesButtonBackground;
            }
            set
            {
                _allLanguagesButtonBackground = value;

                NotifyOfPropertyChange(() => AllLanguagesButtonBackground);
            }
        }

        Brush _allCurrenciesButtonBackground;
        /// <summary>
        /// Background color of the button AllCurrencies
        /// </summary>
        public Brush AllCurrenciesButtonBackground
        {
            get
            {
                return _allCurrenciesButtonBackground;
            }
            set
            {
                _allCurrenciesButtonBackground = value;

                NotifyOfPropertyChange(() => AllCurrenciesButtonBackground);
            }
        }

        Brush _allTimezonesButtonBackground;
        /// <summary>
        /// Background color of the button HomeAlltimeZones
        /// </summary>
        public Brush AllTimezonesButtonBackground
        {
            get
            {
                return _allTimezonesButtonBackground;
            }
            set
            {
                _allTimezonesButtonBackground = value;

                NotifyOfPropertyChange(() => AllTimezonesButtonBackground);
            }
        }

        Brush _allRegionalBlocsButtonBackground;
        /// <summary>
        /// Background color of the button AllRegionalBlocs
        /// </summary>
        public Brush AllRegionalBlocsButtonBackground
        {
            get
            {
                return _allRegionalBlocsButtonBackground;
            }
            set
            {
                _allRegionalBlocsButtonBackground = value;

                NotifyOfPropertyChange(() => AllRegionalBlocsButtonBackground);
            }
        }

        /// <summary>
        /// Default color of disabled buttons
        /// </summary>
        private Brush WhiteSmoke { get; set; } = new SolidColorBrush(Color.FromRgb(245, 245, 245));

        /// <summary>
        /// Default color of enabled buttons
        /// </summary>
        private Brush LightGray { get; set; } = new SolidColorBrush(Color.FromRgb(211, 211, 211));

        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="mainMenuViewModel"></param>
        public HomePageViewModel(MainMenuViewModel mainMenuViewModel)
        {
            this.MainMenuViewModel = mainMenuViewModel;
            mainMenuViewModel.AddressViewName = "Home_Page/";

            Home();
        }

        /// <summary>
        /// Opens The Home View
        /// </summary>
        public void Home()
        {
            HomeButtonBackground = LightGray;
            AllCountriesButtonBackground = WhiteSmoke;
            AllRegionsButtonBackground = WhiteSmoke;
            AllSubregionsButtonBackground = WhiteSmoke;
            AllLanguagesButtonBackground = WhiteSmoke;
            AllCurrenciesButtonBackground = WhiteSmoke;
            AllTimezonesButtonBackground = WhiteSmoke;
            AllRegionalBlocsButtonBackground = WhiteSmoke;


            ActivateItem(new HomePageHomeViewModel(this));
        }

        /// <summary>
        /// Opens The AllCountries View
        /// </summary>
        public void AllCountries()
        {
            HomeButtonBackground = WhiteSmoke;
            AllCountriesButtonBackground = LightGray;
            AllRegionsButtonBackground = WhiteSmoke;
            AllSubregionsButtonBackground = WhiteSmoke;
            AllLanguagesButtonBackground = WhiteSmoke;
            AllCurrenciesButtonBackground = WhiteSmoke;
            AllTimezonesButtonBackground = WhiteSmoke;
            AllRegionalBlocsButtonBackground = WhiteSmoke;


            ActivateItem(new AllCountriesViewModel(MainMenuViewModel));
        }

        /// <summary>
        /// Opens The AllRegions View
        /// </summary>
        public void AllRegions()
        {
            HomeButtonBackground = WhiteSmoke;
            AllCountriesButtonBackground = WhiteSmoke;
            AllRegionsButtonBackground = LightGray;
            AllSubregionsButtonBackground = WhiteSmoke;
            AllLanguagesButtonBackground = WhiteSmoke;
            AllCurrenciesButtonBackground = WhiteSmoke;
            AllTimezonesButtonBackground = WhiteSmoke;
            AllRegionalBlocsButtonBackground = WhiteSmoke;

            ActivateItem(new AllRegionsViewModel(MainMenuViewModel));
        }


        /// <summary>
        /// Opens the All~SubRegions View
        /// </summary>
        public void AllSubRegions()
        {
            HomeButtonBackground = WhiteSmoke;
            AllCountriesButtonBackground = WhiteSmoke;
            AllRegionsButtonBackground = WhiteSmoke;
            AllSubregionsButtonBackground = LightGray;
            AllLanguagesButtonBackground = WhiteSmoke;
            AllCurrenciesButtonBackground = WhiteSmoke;
            AllTimezonesButtonBackground = WhiteSmoke;
            AllRegionalBlocsButtonBackground = WhiteSmoke;

            ActivateItem(new AllSubRegionsViewModel(MainMenuViewModel));
        }

        /// <summary>
        /// Opens the AllLanguages View
        /// </summary>
        public void AllLanguagues()
        {
            HomeButtonBackground = WhiteSmoke;
            AllCountriesButtonBackground = WhiteSmoke;
            AllRegionsButtonBackground = WhiteSmoke;
            AllSubregionsButtonBackground = WhiteSmoke;
            AllLanguagesButtonBackground = LightGray;
            AllCurrenciesButtonBackground = WhiteSmoke;
            AllTimezonesButtonBackground = WhiteSmoke;
            AllRegionalBlocsButtonBackground = WhiteSmoke;

            ActivateItem(new AllLanguaguesViewModel(MainMenuViewModel));
        }

        /// <summary>
        /// Opens the AllCurrencies View
        /// </summary>
        public void AllCurrencies()
        {
            HomeButtonBackground = WhiteSmoke;
            AllCountriesButtonBackground = WhiteSmoke;
            AllRegionsButtonBackground = WhiteSmoke;
            AllSubregionsButtonBackground = WhiteSmoke;
            AllLanguagesButtonBackground = WhiteSmoke;
            AllCurrenciesButtonBackground = LightGray;
            AllTimezonesButtonBackground = WhiteSmoke;
            AllRegionalBlocsButtonBackground = WhiteSmoke;

            ActivateItem(new AllCurrenciesViewModel(MainMenuViewModel));
        }

        /// <summary>
        /// Opens the AllTimeZones View
        /// </summary>
        public void AllTimezones()
        {
            HomeButtonBackground = WhiteSmoke;
            AllCountriesButtonBackground = WhiteSmoke;
            AllRegionsButtonBackground = WhiteSmoke;
            AllSubregionsButtonBackground = WhiteSmoke;
            AllLanguagesButtonBackground = WhiteSmoke;
            AllCurrenciesButtonBackground = WhiteSmoke;
            AllTimezonesButtonBackground = LightGray;
            AllRegionalBlocsButtonBackground = WhiteSmoke;

            ActivateItem(new AllTimezonesViewModel(MainMenuViewModel));
        }

        /// <summary>
        /// Opens the AllRegionalBloc View
        /// </summary>
        public void AllRegionalBlocs()
        {
            HomeButtonBackground = WhiteSmoke;
            AllCountriesButtonBackground = WhiteSmoke;
            AllRegionsButtonBackground = WhiteSmoke;
            AllSubregionsButtonBackground = WhiteSmoke;
            AllLanguagesButtonBackground = WhiteSmoke;
            AllCurrenciesButtonBackground = WhiteSmoke;
            AllTimezonesButtonBackground = WhiteSmoke;
            AllRegionalBlocsButtonBackground = LightGray;

            ActivateItem(new AllRegionalBlocsViewModel(MainMenuViewModel));
        }

    }
}
