﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library;
using World_Library.DATA.MODELS;
using World_Library.DATA.MODELS.World.CountrySUBMODELS;

namespace WPFgui.ViewModels
{
    /// <summary>
    /// AllCurrencies View
    /// </summary>
    public class AllCurrenciesViewModel : Screen
    {

        /// <summary>
        /// Main Menu View
        /// </summary>
        public MainMenuViewModel MainMenuViewModel { get; set; }

        BindableCollection<Currency> _currencies = new BindableCollection<Currency>(WorldController.Currencies.OrderBy(currency => currency.Name));
        /// <summary>
        /// All Unique Currencies
        /// </summary>
        public BindableCollection<Currency> Currencies
        {
            get
            {
                return _currencies;
            }
            set
            {
                _currencies = value;
                NotifyOfPropertyChange(() => Currencies);
            }
        }

        Currency _currency;
        /// <summary>
        /// Selected Currency
        /// </summary>
        public Currency Currency
        {
            get
            {
                return _currency;
            }
            set
            {
                if (value != null)
                {
                    _currency = (Currency)value;

                    NotifyOfPropertyChange(() => Currency);

                    Countries = new BindableCollection<Country>(Currency.Countries);
                    CountryFilterCount = $"Total de Paises: {Countries.Count()}";

                    SearchCountry = SearchCountry;
                }
            }
        }

        BindableCollection<Country> _countries;
        /// <summary>
        /// List of all countries of the same currency selected
        /// </summary>
        public BindableCollection<Country> Countries
        {
            get
            {
                return _countries;
            }
            set
            {
                _countries = value;
                NotifyOfPropertyChange(() => Countries);
            }
        }

        Country _CountrySelected;
        /// <summary>
        /// Opens the country view of selected country
        /// </summary>
        public Country CountrySelected
        {
            get
            {
                return _CountrySelected;
            }
            set
            {
                if (value != null)
                {
                    _CountrySelected = (Country)value;

                    NotifyOfPropertyChange(() => CountrySelected);

                    MainMenuViewModel.ActivateItem(new CountryMainPageViewModel(MainMenuViewModel, CountrySelected));
                }
            }
        }

        string _countryFilterCount;
        /// <summary>
        /// Counts how many country are in this filter
        /// </summary>
        public string CountryFilterCount
        {
            get
            {
                return _countryFilterCount;
            }
            set
            {
                _countryFilterCount = value;

                NotifyOfPropertyChange(() => CountryFilterCount);
            }
        }

        string _searchCountry = "";
        /// <summary>
        /// Updates the Countries list based on search country
        /// </summary>
        public string SearchCountry
        {
            get
            {
                return _searchCountry;
            }
            set
            {
                _searchCountry = value;

                NotifyOfPropertyChange(() => value);

                if (Currency != null)
                {
                    Countries = new BindableCollection<Country>(Currency.Countries.Where(country => country.Name.ToLower().StartsWith(value.ToLower())));
                    CountryFilterCount = $"Total de Paises: {Countries.Count()}";

                }

            }
        }

        /// <summary>
        /// Contructor in Main Page
        /// </summary>
        /// <param name="mainMenuViewModel">Main Page View</param>
        public AllCurrenciesViewModel(MainMenuViewModel mainMenuViewModel)
        {
            this.MainMenuViewModel = mainMenuViewModel;

            MainMenuViewModel.AddressViewName = "Home_Page/Currencies";


        }

        /// <summary>
        /// Contructor based on Country View
        /// </summary>
        /// <param name="mainMenuViewModel">Main Page View</param>
        /// <param name="selectedCountry">Country from where this call came from</param>
        /// <param name="selectedCurrency">Currency that was selected</param>
        public AllCurrenciesViewModel(MainMenuViewModel mainMenuViewModel, Country selectedCountry, Currency selectedCurrency)
        {
            this.MainMenuViewModel = mainMenuViewModel;

            MainMenuViewModel.AddressViewName = "Home_Page/Currencies";

            this.Currency = selectedCurrency;
            this.SearchCountry = selectedCountry.Name;


        }

    }
}
