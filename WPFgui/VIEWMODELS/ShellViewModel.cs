﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFgui.ViewModels
{
    /// <summary>
    /// Main ViewModel
    /// </summary>
    public class ShellViewModel : Conductor<object>
    {

        /// <summary>
        /// Contructor
        /// </summary>
        public ShellViewModel()
        {
            LoadLoading();
        }

        /// <summary>
        /// Opens the Loading View
        /// </summary>
        private void LoadLoading()
        {    
            ActivateItem(new LoadingViewModel(this));
        }

        /// <summary>
        /// Opens the Main Menu View
        /// </summary>
        internal void LoadMainMenu()
        {
            ActivateItem(new MainMenuViewModel());
        }
    }
}
