﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using World_Library.DATA.MODELS;
using World_Library.DATA.MODELS.World.CountrySUBMODELS;

namespace WPFgui.ViewModels
{
    /// <summary>
    /// Country Main Page View
    /// </summary>
    public class CountryMainPageViewModel : Conductor<object>
    {
        /// <summary>
        /// Main Menu View
        /// </summary>
        public MainMenuViewModel MainMenuView { get; set; }

        /// <summary>
        /// The country that has been selected
        /// </summary>
        public Country Country { get; set; }

        Brush _homeButtonBackground;
        /// <summary>
        /// The Home Button Background Color
        /// </summary>
        public Brush HomeButtonBackground
        {
            get
            {
                return _homeButtonBackground;
            }
            set
            {
                _homeButtonBackground = value;

                NotifyOfPropertyChange( () => HomeButtonBackground);
            }
        }

        Brush _detailsButtonBackground;
        /// <summary>
        /// The DEtails Button Background Color
        /// </summary>
        public Brush DetailsButtonBackground
        {
            get
            {
                return _detailsButtonBackground;
            }
            set
            {
                _detailsButtonBackground = value;

                NotifyOfPropertyChange(() => DetailsButtonBackground);
            }
        }

        /// <summary>
        /// Default color for Enabled Buttons
        /// </summary>
        private Brush EnabledButton { get; set; } = new SolidColorBrush(Color.FromRgb(211, 211, 211));

        /// <summary>
        /// Default color for disabled Buttons
        /// </summary>
        private Brush DisabledButton { get; set; } = new SolidColorBrush(Color.FromRgb(245, 245, 245));

        /// <summary>
        /// Construcot
        /// </summary>
        /// <param name="mainMenuViewModel">Receives the main View</param>
        /// <param name="countrySelected">Receives the country that has been selected</param>
        public CountryMainPageViewModel(MainMenuViewModel mainMenuViewModel, Country countrySelected)
        {
            Country = countrySelected;
            MainMenuView = mainMenuViewModel;

            MainMenuView.AddressViewName = $"{Country.Name}/";

            Home();
        }

        /// <summary>
        /// Opens the Country home View
        /// </summary>
        public void Home()
        {
            HomeButtonBackground = EnabledButton;
            DetailsButtonBackground = DisabledButton;

            ActivateItem(new CountryHomeViewModel(MainMenuView,Country));
        }

        /// <summary>
        /// Opens the Country Details View
        /// </summary>
        public void Details()
        {
            HomeButtonBackground = DisabledButton;
            DetailsButtonBackground = EnabledButton;

            ActivateItem(new CountryDetailsViewModel(MainMenuView,Country, this));
        }

        /// <summary>
        /// Opens the AllRegion View with the region and country selected
        /// </summary>
        /// <param name="RegionSelected">Selected REgion</param>
        public void Region(Region RegionSelected)
        {
            HomeButtonBackground = DisabledButton;
            DetailsButtonBackground = DisabledButton;

            ActivateItem(new AllRegionsViewModel(MainMenuView, Country ,RegionSelected));
        }

        /// <summary>
        /// Opens the AllSubREgion View with the subregion and country selected
        /// </summary>
        /// <param name="subRegion">Selected SubREgion</param>
        public void SubRegion(SubRegion subRegion)
        {
            HomeButtonBackground = DisabledButton;
            DetailsButtonBackground = DisabledButton;

            ActivateItem(new AllSubRegionsViewModel(MainMenuView, Country ,subRegion));
        }

        /// <summary>
        /// Opens the AllTimeZones View with the timezone and country selected
        /// </summary>
        /// <param name="timeZone">SelectedTimezone</param>
        public void TimeZone(CustomTimeZone timeZone)
        {
            HomeButtonBackground = DisabledButton;
            DetailsButtonBackground = DisabledButton;

            ActivateItem(new AllTimezonesViewModel(MainMenuView, Country, timeZone));
        }

        /// <summary>
        /// Opens the Country view of the selected border
        /// </summary>
        /// <param name="country">selected Border(Country)</param>
        public void Border(Country country)
        {
            HomeButtonBackground = DisabledButton;
            DetailsButtonBackground = DisabledButton;

            MainMenuView.ActivateItem(new CountryMainPageViewModel(MainMenuView,country));
        }

        /// <summary>
        /// Opens The AllCurrencies View of the selected currency and country
        /// </summary>
        /// <param name="currency">Selected Currency</param>
        public void Currency(Currency currency)
        {
            HomeButtonBackground = DisabledButton;
            DetailsButtonBackground = DisabledButton;

            ActivateItem(new AllCurrenciesViewModel(MainMenuView,Country,currency));
        }

        /// <summary>
        /// Opens the AllLanguages View of the selected language and country
        /// </summary>
        /// <param name="language">Selected Language</param>
        public void Language(Language language)
        {
            HomeButtonBackground = DisabledButton;
            DetailsButtonBackground = DisabledButton;

            ActivateItem(new AllLanguaguesViewModel(MainMenuView, Country, language));
        }

        /// <summary>
        /// Opens the AllRegionalBlocs of the selected regionalbloc and country
        /// </summary>
        /// <param name="regionalBloc">Selected REgionalBloc</param>
        public void RegionalBloc(RegionalBloc regionalBloc)
        {
            HomeButtonBackground = DisabledButton;
            DetailsButtonBackground = DisabledButton;

            ActivateItem(new AllRegionalBlocsViewModel(MainMenuView, Country, regionalBloc));
        }
    }
}
