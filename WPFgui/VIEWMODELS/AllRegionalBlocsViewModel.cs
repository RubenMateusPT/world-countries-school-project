﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library;
using World_Library.DATA.MODELS;
using World_Library.DATA.MODELS.World.CountrySUBMODELS;

namespace WPFgui.ViewModels
{
    /// <summary>
    /// All REgionalBlocs View
    /// </summary>
    public class AllRegionalBlocsViewModel : Screen
    {
        /// <summary>
        /// Main Menu View
        /// </summary>
        public MainMenuViewModel MainMenuViewModel { get; set; }

        BindableCollection<RegionalBloc> _regionBloc = new BindableCollection<RegionalBloc>(WorldController.RegionalBlocs.OrderBy(regionalbloc => regionalbloc.Name));
        /// <summary>
        /// All Unique REgionalBloc
        /// </summary>
        public BindableCollection<RegionalBloc> RegionsBlocs
        {
            get
            {
                return _regionBloc;
            }
            set
            {
                _regionBloc = value;
                NotifyOfPropertyChange(() => RegionsBlocs);
            }
        }

        RegionalBloc _regionBlocSelected;
        /// <summary>
        /// Selected RegionalBloc
        /// </summary>
        public RegionalBloc RegionBlocsSelected
        {
            get
            {
                return _regionBlocSelected;
            }
            set
            {
                if (value != null)
                {
                    _regionBlocSelected = (RegionalBloc)value;

                    NotifyOfPropertyChange(() => RegionBlocsSelected);

                    Countries = new BindableCollection<Country>(RegionBlocsSelected.Countries);
                    CountryFilterCount = $"Total de Paises: {Countries.Count()}";

                    SearchCountry = SearchCountry;
                }
            }
        }

        BindableCollection<Country> _countries;
        /// <summary>
        /// List of all countries of the same REgionalBloc selected
        /// </summary>
        public BindableCollection<Country> Countries
        {
            get
            {
                return _countries;
            }
            set
            {
                _countries = value;
                NotifyOfPropertyChange(() => Countries);
            }
        }

        Country _CountrySelected;
        /// <summary>
        /// Opens the country view of selected country
        /// </summary>
        public Country CountrySelected
        {
            get
            {
                return _CountrySelected;
            }
            set
            {
                if (value != null)
                {
                    _CountrySelected = (Country)value;

                    NotifyOfPropertyChange(() => CountrySelected);

                    MainMenuViewModel.ActivateItem(new CountryMainPageViewModel(MainMenuViewModel, CountrySelected));
                }
            }
        }

        string _countryFilterCount;
        /// <summary>
        /// Counts how many country are in this filter
        /// </summary>
        public string CountryFilterCount
        {
            get
            {
                return _countryFilterCount;
            }
            set
            {
                _countryFilterCount = value;

                NotifyOfPropertyChange(() => CountryFilterCount);
            }
        }

        string _searchCountry = "";
        /// <summary>
        /// Updates the Countries list based on search country
        /// </summary>
        public string SearchCountry
        {
            get
            {
                return _searchCountry;
            }
            set
            {
                _searchCountry = value;

                NotifyOfPropertyChange(() => value);

                if (RegionBlocsSelected != null)
                {
                    Countries = new BindableCollection<Country>(RegionBlocsSelected.Countries.Where(country => country.Name.ToLower().StartsWith(value.ToLower())));
                    CountryFilterCount = $"Total de Paises: {Countries.Count()}";
                }

            }
        }

        /// <summary>
        /// Contructor in Main Page
        /// </summary>
        /// <param name="mainMenuViewModel">Main Page View</param>
        public AllRegionalBlocsViewModel(MainMenuViewModel mainMenuViewModel)
        {
            this.MainMenuViewModel = mainMenuViewModel;

            MainMenuViewModel.AddressViewName = "Home_Page/Regional_Blocs";


        }

        /// <summary>
        /// Contructor based on Country View
        /// </summary>
        /// <param name="mainMenuViewModel">Main Page View</param>
        /// <param name="selectedCountry">Country from where this call came from</param>
        /// <param name="regionalBloc">REgional bloc that was selected</param>
        public AllRegionalBlocsViewModel(MainMenuViewModel mainMenuViewModel, Country selectedCountry, RegionalBloc regionalBloc)
        {
            this.MainMenuViewModel = mainMenuViewModel;

            MainMenuViewModel.AddressViewName = "Home_Page/Regional_Blocs";

            this.RegionBlocsSelected = regionalBloc;
            this.SearchCountry = selectedCountry.Name;
        }
    }
}
