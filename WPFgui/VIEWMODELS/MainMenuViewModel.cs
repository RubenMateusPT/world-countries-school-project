﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using World_Library;
using World_Library.DATA.MODELS;
using World_Library.SERVICES;

namespace WPFgui.ViewModels
{
    /// <summary>
    /// Main Menu View model
    /// </summary>
    public class MainMenuViewModel : Conductor<object>
    {

        string _statusMessage;
        /// <summary>
        /// Status Message that appears whenever theres a backend change
        /// </summary>
        public string StatusMessage
        {
            get
            {
                return _statusMessage;
            }
            set
            {
                _statusMessage = value;
                NotifyOfPropertyChange( () => StatusMessage);
                
            }
        }

        Brush _networkStatus;
        /// <summary>
        /// Verifies and changes the networkStatus icon
        /// </summary>
        public Brush NetworkStatus
        {
            get
            {
                return _networkStatus;
            }
            set
            {
                _networkStatus = value;
                NotifyOfPropertyChange(() => NetworkStatus);
            }
        }

        string _networkStatusMessage;
        /// <summary>
        /// Displays a message of the netword status
        /// </summary>
        public string NetworkStatusMessage
        {
            get
            {
                return _networkStatusMessage;
            }
            set
            {
                _networkStatusMessage = value;
                NotifyOfPropertyChange(() => NetworkStatusMessage);
            }
        }

        string _addressViewName;
        /// <summary>
        /// Changes de end of the "URL"
        /// </summary>
        public string AddressViewName
        {
            get { return _addressViewName; }
            set
            {
                _addressViewName = value;
                NotifyOfPropertyChange( () => AddressViewName);
            }
        }

        Visibility _searchPlaceHolder;
        /// <summary>
        /// Chancge the visibility of the searchPlaceholder
        /// </summary>
        public Visibility SearchPlaceHolder
        {
            get
            {
                return _searchPlaceHolder;
            }
            set
            {
                _searchPlaceHolder = value;
                NotifyOfPropertyChange( () => SearchPlaceHolder);
            }
        }

        Visibility _searchArrow = Visibility.Hidden;
        /// <summary>
        /// Changes the visibility of the searchArrow
        /// </summary>
        public Visibility SearchArrow
        {
            get
            {
                return _searchArrow;
            }
            set
            {
                _searchArrow = value;
                NotifyOfPropertyChange(() => SearchArrow);
            }
        }

        Visibility _dropdownSearch = Visibility.Hidden;
        /// <summary>
        /// Changes the visibility of the dropdown menu
        /// </summary>
        public Visibility DropdownSearch
        {
            get
            {
                return _dropdownSearch;
            }
            set
            {
                _dropdownSearch = value;
                NotifyOfPropertyChange(() => DropdownSearch);
            }
        }

        Thickness _dropdownSearchMargin;
        /// <summary>
        /// Changes the size of the Dropdown Menu based on how many countries are displayed
        /// </summary>
        public Thickness DropdownSearchMargin
        {
            get
            {
                return _dropdownSearchMargin;
            }
            set
            {
                _dropdownSearchMargin = value;

                NotifyOfPropertyChange( () => DropdownSearchMargin);
            }
        }

        string _searchCountry = "";
        /// <summary>
        /// Checks what is being written on the SearchBar
        /// </summary>
        public string SearchCountry
        {
            get
            {
                return _searchCountry;
            }
            set
            {
                _searchCountry = value;

                DropdownSearch = Visibility.Visible;

                if (string.IsNullOrEmpty(_searchCountry))
                {
                    SearchPlaceHolder = Visibility.Visible;
                    SearchArrow = Visibility.Hidden;
                }
                else
                {
                    SearchPlaceHolder = Visibility.Hidden;
                    SearchArrow = Visibility.Visible;
                }

                NotifyOfPropertyChange( () => value );

                Countries = new BindableCollection<Country>(WorldController.Countries.Where(country => country.Name.ToLower().StartsWith(value.ToLower())));

                if(Countries.Count <= 10)
                {
                    int bottomMarginSize = Countries.Count() * -28;

                    DropdownSearchMargin = new Thickness(0, -2, -0, bottomMarginSize);
                }
                else
                {
                    DropdownSearchMargin = new Thickness(0, -2, 0, -280);
                }

            }
        }

        BindableCollection<Country> _countries;
        /// <summary>
        /// Retrieves the list of countries based on the search bar
        /// </summary>
        public BindableCollection<Country> Countries
        {
            get
            {
                return _countries;
            }
            set
            {
                _countries = value;
                NotifyOfPropertyChange( () => Countries);
            }
        }

        Country _CountrySelected;
        /// <summary>
        /// Opens the view of the country selected from the dropdown menu
        /// </summary>
        public Country CountrySelected
        {
            get
            {
                return _CountrySelected;
            }
            set
            {
                _CountrySelected = (Country)value;

                NotifyOfPropertyChange(() => CountrySelected);

                if (value != null)
                {

                    ActivateItem(new CountryMainPageViewModel(this, CountrySelected));
                }
            }
        }

        /// <summary>
        /// Controls the windows displayed on the pogram
        /// </summary>
        public WindowManager WindowManager { get; set; } = new WindowManager();

        /// <summary>
        /// Controls the timer to execute at interval times an action
        /// </summary>
        DispatcherTimer dispatcherTimer = new DispatcherTimer();

        /// <summary>
        /// Contructor
        /// </summary>
        public MainMenuViewModel()
        {
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0,0,10);
            dispatcherTimer.Start();

            dispatcherTimer_Tick(null,null);

            HomePage();
        }

        /// <summary>
        /// Timer Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            ServiceResponseModel networkReponse = new ServiceResponseModel();
            await Task.Run(() => networkReponse = new NetworkService().CheckConnection());
            if (networkReponse.Success)
            {
                NetworkStatus = new SolidColorBrush(Color.FromRgb(0, 225, 0));
                NetworkStatusMessage = "Com conexão à Internet";
            }
            else
            {
                NetworkStatus = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                NetworkStatusMessage = "Sem ligação à internet";
            }
        }

        /// <summary>
        /// Makes Visibile the dropdown menu
        /// </summary>
        public void SearchBarOnFocus()
        {
            SearchCountry = SearchCountry;
            DropdownSearch = Visibility.Visible;
        }

        /// <summary>
        /// Hides the dropdown menu
        /// </summary>
        public void SearchBarLostFocus()
        {
            CountrySelected = null;

            DropdownSearch = Visibility.Hidden;
        }

        /// <summary>
        /// Opens the HomePage View
        /// </summary>
        public void HomePage()
        {
            ActivateItem(new HomePageViewModel(this));
        }

        /// <summary>
        /// Opens the about page in a new window
        /// </summary>
        public void About()
        {
            WindowManager.ShowDialog(new AboutViewModel());
        }
    }
}
