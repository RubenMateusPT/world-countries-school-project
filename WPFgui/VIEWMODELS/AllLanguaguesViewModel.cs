﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library;
using World_Library.DATA.MODELS;
using World_Library.DATA.MODELS.World.CountrySUBMODELS;

namespace WPFgui.ViewModels
{
    /// <summary>
    /// AllLanguages View
    /// </summary>
    public class AllLanguaguesViewModel : Screen
    {
        /// <summary>
        /// Main Menu View
        /// </summary>
        public MainMenuViewModel MainMenuViewModel { get; set; }

        BindableCollection<Language> _languages = new BindableCollection<Language>(WorldController.Languages.OrderBy(language => language.Name));
        /// <summary>
        /// All Unique Languages
        /// </summary>
        public BindableCollection<Language> Languages
        {
            get
            {
                return _languages;
            }
            set
            {
                _languages = value;
                NotifyOfPropertyChange(() => Languages);
            }
        }

        Language _languageSelected;
        /// <summary>
        /// /// <summary>
        /// Selected Languague
        /// </summary>
        /// </summary>
        public Language Language
        {
            get
            {
                return _languageSelected;
            }
            set
            {
                if (value != null)
                {
                    _languageSelected = (Language)value;

                    NotifyOfPropertyChange(() => Language);

                    Countries = new BindableCollection<Country>(Language.Countries);
                    CountryFilterCount = $"Total de Paises: {Countries.Count()}";

                    SearchCountry = SearchCountry;
                }
            }
        }

        BindableCollection<Country> _countries;
        /// <summary>
        /// List of all countries of the same Language selected
        /// </summary>
        public BindableCollection<Country> Countries
        {
            get
            {
                return _countries;
            }
            set
            {
                _countries = value;
                NotifyOfPropertyChange(() => Countries);
            }
        }

        Country _CountrySelected;
        /// <summary>
        /// Opens the country view of selected country
        /// </summary>
        public Country CountrySelected
        {
            get
            {
                return _CountrySelected;
            }
            set
            {
                if (value != null)
                {
                    _CountrySelected = (Country)value;

                    NotifyOfPropertyChange(() => CountrySelected);

                    MainMenuViewModel.ActivateItem(new CountryMainPageViewModel(MainMenuViewModel, CountrySelected));
                }
            }
        }

        string _countryFilterCount;
        /// <summary>
        /// Counts how many country are in this filter
        /// </summary>
        public string CountryFilterCount
        {
            get
            {
                return _countryFilterCount;
            }
            set
            {
                _countryFilterCount = value;

                NotifyOfPropertyChange(() => CountryFilterCount);
            }
        }

        string _searchCountry = "";
        /// <summary>
        /// Updates the Countries list based on search country
        /// </summary>
        public string SearchCountry
        {
            get
            {
                return _searchCountry;
            }
            set
            {
                _searchCountry = value;

                NotifyOfPropertyChange(() => value);

                if (Language != null)
                {
                    Countries = new BindableCollection<Country>(Language.Countries.Where(country => country.Name.ToLower().StartsWith(value.ToLower())));
                    CountryFilterCount = $"Total de Paises: {Countries.Count()}";
                }

            }
        }

        /// <summary>
        /// Contructor in Main Page
        /// </summary>
        /// <param name="mainMenuViewModel">Main Page View</param>
        public AllLanguaguesViewModel(MainMenuViewModel mainMenuViewModel)
        {
            this.MainMenuViewModel = mainMenuViewModel;

            MainMenuViewModel.AddressViewName = "Home_Page/Languages";


        }

        /// <summary>
        /// Contructor based on Country View
        /// </summary>
        /// <param name="mainMenuViewModel">Main Page View</param>
        /// <param name="selectedCountry">Country from where this call came from</param>
        /// <param name="selectedLangauge">Language that was selected</param>
        public AllLanguaguesViewModel(MainMenuViewModel mainMenuViewModel, Country selectedCountry, Language selectedLangauge)
        {
            this.MainMenuViewModel = mainMenuViewModel;

            MainMenuViewModel.AddressViewName = "Home_Page/Languages";

            this.Language = selectedLangauge;

            this.SearchCountry = selectedCountry.Name;

        }
    }
}
