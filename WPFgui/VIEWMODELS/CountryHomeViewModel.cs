﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library.DATA.MODELS;

namespace WPFgui.ViewModels
{
    /// <summary>
    /// Country Home View
    /// </summary>
    public class CountryHomeViewModel : Screen
    {
        /// <summary>
        /// Main Menu View
        /// </summary>
        public MainMenuViewModel MainMenuView { get; set; }
        /// <summary>
        /// Country Selected
        /// </summary>
        public Country Country { get; set; }

        string _name;
        /// <summary>
        /// Country Name
        /// </summary>
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;

                NotifyOfPropertyChange( () => Name);
            }
        }

        string _nativeName;
        /// <summary>
        /// Country NativeName
        /// </summary>
        public string NativeName
        {
            get
            {
                return _nativeName;
            }
            set
            {
                _nativeName = value;

                NotifyOfPropertyChange( () => NativeName);
            }
        }

        string _flag;
        /// <summary>
        /// Country Flag
        /// </summary>
        public string Flag
        {
            get
            {
                return _flag;
            }
            set
            {
                _flag = value;

                NotifyOfPropertyChange( () => Flag);
            }
        }

        string _regionSubregion;
        /// <summary>
        /// Country Region and SubRegion
        /// </summary>
        public string RegionSubregion
        {
            get
            {
                return _regionSubregion;
            }
            set
            {
                _regionSubregion = value;

                NotifyOfPropertyChange(() => RegionSubregion);
            }
        }

        string _population;
        /// <summary>
        /// Country Population
        /// </summary>
        public string Population
        {
            get
            {
                return _population;
            }
            set
            {
                _population = value;

                NotifyOfPropertyChange( () => Population);
            }
        }

        string _gini;
        /// <summary>
        /// Country Gini
        /// </summary>
        public string Gini
        {
            get
            {
                return _gini;
            }
            set
            {
                _gini = value;

                NotifyOfPropertyChange( () => Gini );
            }
        }

        /// <summary>
        /// Constructor of view
        /// </summary>
        /// <param name="mainMenuView">AMin Menu View</param>
        /// <param name="country">Country that has beeen selected</param>
        public CountryHomeViewModel(MainMenuViewModel mainMenuView, Country country)
        {
            this.Country = country;
            MainMenuView = mainMenuView;

            MainMenuView.AddressViewName = $"{Country.Name}/Home_Page/";

            LoadCountryInformation();
        }

        /// <summary>
        /// Loads the country information to this view properties
        /// </summary>
        private void LoadCountryInformation()
        {
            Name = $"{Country.Name}";

            if(Country.Capital != null && !string.IsNullOrEmpty(Country.Capital))
            {
                Name += $" ({Country.Capital})";
            }
            else
            {
                Name += " (N/A)";
            }

            NativeName = Country.NativeName;

            Flag = Country.Flag;

            if(Country.Region != null && !string.IsNullOrEmpty(Country.Region.Name))
            {
                RegionSubregion = $"{Country.Region.Name}";
            }
            else
            {
                RegionSubregion = "N/A";
            }

            if (Country.SubRegion != null && !string.IsNullOrEmpty(Country.SubRegion.Name))
            {
                RegionSubregion += $"-> {Country.SubRegion.Name}";
            }
            else
            {
                RegionSubregion += "-> N/A";
            }

            Population = $"{Country.Population} Habitantes";

            if (Country.Gini != null)
            {
                Gini = $"{Country.Gini} Gini";
            }
        }
    }
}
