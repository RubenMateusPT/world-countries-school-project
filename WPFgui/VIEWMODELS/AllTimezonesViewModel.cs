﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library.DATA.MODELS;
using World_Library.DATA.MODELS.World.CountrySUBMODELS;

namespace WPFgui.ViewModels
{
    /// <summary>
    /// All TimeZones View
    /// </summary>
    public class AllTimezonesViewModel : Screen
    {
        /// <summary>
        /// Main Menu View
        /// </summary>
        public MainMenuViewModel MainMenuViewModel { get; set; }

        BindableCollection<CustomTimeZone> _timeZones = new BindableCollection<CustomTimeZone>(World_Library.WorldController.CustomTimeZones.OrderBy(timezone => timezone.ToString()));
        /// <summary>
        /// List of all unique timezones
        /// </summary>
        public BindableCollection<CustomTimeZone> TimeZones
        {
            get
            {
                return _timeZones;
            }
            set
            {
                _timeZones = value;
                NotifyOfPropertyChange(() => TimeZones);
            }
        }

        CustomTimeZone _timeZone;
        /// <summary>
        /// Selected timezone
        /// </summary>
        public CustomTimeZone TimeZone
        {
            get
            {
                return _timeZone;
            }
            set
            {
                if (value != null)
                {
                    _timeZone = (CustomTimeZone)value;

                    NotifyOfPropertyChange(() => TimeZone);

                    Countries = new BindableCollection<Country>(TimeZone.Countries);
                    CountryFilterCount = $"Total de Paises: {Countries.Count()}";

                    SearchCountry = SearchCountry;
                }
            }
        }

        BindableCollection<Country> _countries;
        /// <summary>
        /// List of all countries of the same timezone selected
        /// </summary>
        public BindableCollection<Country> Countries
        {
            get
            {
                return _countries;
            }
            set
            {
                _countries = value;
                NotifyOfPropertyChange(() => Countries);
            }
        }

        Country _CountrySelected;
        /// <summary>
        /// Opens the country view of selected country
        /// </summary>
        public Country CountrySelected
        {
            get
            {
                return _CountrySelected;
            }
            set
            {
                if (value != null)
                {
                    _CountrySelected = (Country)value;

                    NotifyOfPropertyChange(() => CountrySelected);

                    MainMenuViewModel.ActivateItem(new CountryMainPageViewModel(MainMenuViewModel, CountrySelected));
                }
            }
        }

        string _countryFilterCount;
        /// <summary>
        /// Counts how many country are in this filter
        /// </summary>
        public string CountryFilterCount
        {
            get
            {
                return _countryFilterCount;
            }
            set
            {
                _countryFilterCount = value;

                NotifyOfPropertyChange(() => CountryFilterCount);
            }
        }

        string _searchCountry = "";
        /// <summary>
        /// Updates the Countries list based on search country
        /// </summary>
        public string SearchCountry
        {
            get
            {
                return _searchCountry;
            }
            set
            {
                _searchCountry = value;

                NotifyOfPropertyChange(() => value);

                if (TimeZone != null)
                {
                    Countries = new BindableCollection<Country>(TimeZone.Countries.Where(country => country.Name.ToLower().StartsWith(value.ToLower())));
                    CountryFilterCount = $"Total de Paises: {Countries.Count()}";
                }

            }
        }

        /// <summary>
        /// Contructor in Main Page
        /// </summary>
        /// <param name="mainMenuViewModel">Main Page View</param>
        public AllTimezonesViewModel(MainMenuViewModel mainMenuViewModel)
        {
            this.MainMenuViewModel = mainMenuViewModel;

            MainMenuViewModel.AddressViewName = "Home_Page/TimeZones";


        }

        /// <summary>
        /// Contructor based on Country View
        /// </summary>
        /// <param name="mainMenuViewModel">Main Page View</param>
        /// <param name="selectedCountry">Country from where this call came from</param>
        /// <param name="customTimeZone">TimeZone that was selected</param>
        public AllTimezonesViewModel(MainMenuViewModel mainMenuViewModel, Country selectedCountry, CustomTimeZone customTimeZone)
        {
            this.MainMenuViewModel = mainMenuViewModel;

            MainMenuViewModel.AddressViewName = "Home_Page/TimeZones";

            this.TimeZone = customTimeZone;
            this.SearchCountry = selectedCountry.Name;
        }

    }
}
