﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using World_Library;
using World_Library.SERVICES;
using World_Library.SERVICES.MODELS;
using WPFgui.Views;

namespace WPFgui.ViewModels
{
    /// <summary>
    /// Loading View 
    /// </summary>
    public class LoadingViewModel : Screen
    { 

        string _LogMessage;
        /// <summary>
        /// Show the message obtained by the backend
        /// </summary>
        public string LogMessage {
            get
            {
                return _LogMessage;
            }
            set
            {
                _LogMessage += string.Format(value);

                NotifyOfPropertyChange( () => LogMessage );
            }
        }

        double _progressValue;
        /// <summary>
        /// Retrieves the percentage of how much was loaded
        /// </summary>
        public double ProgressBar
        {
            get
            {
                return _progressValue;
            }

            set
            {
                _progressValue += value;

                NotifyOfPropertyChange( () => ProgressBar);
            }
        }

        /// <summary>
        /// View of ShellView
        /// </summary>
        private ShellViewModel ShellViewModel { get; set; }

        /// <summary>
        /// Timer that works as a countdown
        /// </summary>
        private DispatcherTimer SecondsCounter { get; set; } = new DispatcherTimer();

        /// <summary>
        /// Sees if the network is reachable or not
        /// </summary>
        private bool NetworkAvaible { get; set; }

        /// <summary>
        /// How many seconds the countdown timer counts
        /// </summary>
        private int SecondsTimer { get; set; } = 3;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="shellViewModel"></param>
        public LoadingViewModel(ShellViewModel shellViewModel)
        {
            this.ShellViewModel = shellViewModel;

            SecondsCounter.Tick += new EventHandler(secondsCounter);
            SecondsCounter.Interval = new TimeSpan(0,0,1);
        }

        /// <summary>
        /// Calls the creation of the world
        /// </summary>
        public async void StartLoading()
        {
            Progress<ReportModel> progress = new Progress<ReportModel>();
            progress.ProgressChanged += ReportProgress;

            ServiceResponseModel worldService = await WorldController.CreateWorld(progress);
            NetworkAvaible = worldService.Success;

            if (!worldService.Success)
            {
                LogMessage = "A aplicação irá encerrar em breve \n";
            }
            else
            {
                LogMessage = "A abrir Menu Principal \n";
            }

            SecondsCounter.Start();
        }

        /// <summary>
        /// Event that will happen when the countdown timer reaches 0
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void secondsCounter(object sender, EventArgs e)
        {
            if (!NetworkAvaible)
            {
                LogMessage = $"A fechar applicação em {SecondsTimer}...\n";

                if(SecondsTimer == 0)
                {
                    Environment.Exit(0);
                }
            }
            else
            {
                LogMessage = $"A abrir o menu principal em {SecondsTimer}...\n";

                if(SecondsTimer == 0)
                {
                    ShellViewModel.LoadMainMenu();
                    this.TryClose();
                }
            }
            SecondsTimer--;
        }

        

        private void ReportProgress(object sender, ReportModel reportModel)
        {
            LogMessage = reportModel.Message;
            ProgressBar = reportModel.Percentage;
        }
    }
}
