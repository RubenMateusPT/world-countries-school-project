﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World_Library;
using World_Library.DATA.MODELS;
using World_Library.DATA.MODELS.World.CountrySUBMODELS;

namespace WPFgui.ViewModels
{
    /// <summary>
    /// All SubRegions View
    /// </summary>
    public class AllSubRegionsViewModel : Screen
    {
        /// <summary>
        /// Main Menu View
        /// </summary>
        public MainMenuViewModel MainMenuViewModel { get; set; }

        BindableCollection<SubRegion> _subRegions = new BindableCollection<SubRegion>(WorldController.SubRegions.OrderBy(subregion => subregion.Name));
        /// <summary>
        /// All Unique SubRegions
        /// </summary>
        public BindableCollection<SubRegion> Subregions
        {
            get
            {
                return _subRegions;
            }
            set
            {
                _subRegions = value;
                NotifyOfPropertyChange(() => Subregions);
            }
        }

        SubRegion _subRegionSelected;
        /// <summary>
        /// Selected SubREgion
        /// </summary>
        public SubRegion SubRegionSelected
        {
            get
            {
                return _subRegionSelected;
            }
            set
            {
                if (value != null)
                {
                    _subRegionSelected = (SubRegion)value;

                    NotifyOfPropertyChange(() => SubRegionSelected);

                    Countries = new BindableCollection<Country>(SubRegionSelected.Countries);
                    CountryFilterCount = $"Total de Paises: {Countries.Count()}";

                    SearchCountry = SearchCountry;
                }
            }
        }

        BindableCollection<Country> _countries;
        /// <summary>
        /// List of all countries of the same subregion selected
        /// </summary>
        public BindableCollection<Country> Countries
        {
            get
            {
                return _countries;
            }
            set
            {
                _countries = value;
                NotifyOfPropertyChange(() => Countries);
            }
        }

        Country _CountrySelected;
        /// <summary>
        /// Opens the country view of selected country
        /// </summary>
        public Country CountrySelected
        {
            get
            {
                return _CountrySelected;
            }
            set
            {
                if (value != null)
                {
                    _CountrySelected = (Country)value;

                    NotifyOfPropertyChange(() => CountrySelected);

                    MainMenuViewModel.ActivateItem(new CountryMainPageViewModel(MainMenuViewModel, CountrySelected));
                }
            }
        }

        string _countryFilterCount;
        /// <summary>
        /// Counts how many country are in this filter
        /// </summary>
        public string CountryFilterCount
        {
            get
            {
                return _countryFilterCount;
            }
            set
            {
                _countryFilterCount = value;

                NotifyOfPropertyChange(() => CountryFilterCount);
            }
        }

        string _searchCountry = "";
        /// <summary>
        /// Updates the Countries list based on search country
        /// </summary>
        public string SearchCountry
        {
            get
            {
                return _searchCountry;
            }
            set
            {
                _searchCountry = value;

                NotifyOfPropertyChange(() => value);

                if (SubRegionSelected != null)
                {
                    Countries = new BindableCollection<Country>(SubRegionSelected.Countries.Where(country => country.Name.ToLower().StartsWith(value.ToLower())));
                    CountryFilterCount = $"Total de Paises: {Countries.Count()}";
                }

            }
        }

        /// <summary>
        /// Contructor in Main Page
        /// </summary>
        /// <param name="mainMenuViewModel">Main Page View</param>
        public AllSubRegionsViewModel(MainMenuViewModel mainMenuViewModel)
        {
            this.MainMenuViewModel = mainMenuViewModel;

            MainMenuViewModel.AddressViewName = "Home_Page/SubRegions";


        }

        /// <summary>
        /// Contructor based on Country View
        /// </summary>
        /// <param name="mainMenuViewModel">Main Page View</param>
        /// <param name="CountrySelected">Country from where this call came from</param>
        /// <param name="SubRegionSelected">Subregion that was selected</param>
        public AllSubRegionsViewModel(MainMenuViewModel mainMenuViewModel, Country CountrySelected,SubRegion SubRegionSelected)
        {
            this.MainMenuViewModel = mainMenuViewModel;

            MainMenuViewModel.AddressViewName = "Home_Page/SubRegions";

            this.SubRegionSelected = SubRegionSelected;
            this.SearchCountry = CountrySelected.Name;


        }
    }
}
