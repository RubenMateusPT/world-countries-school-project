﻿namespace WPFgui
{
    using Caliburn.Micro;
    using System.Threading.Tasks;
    using System.Windows;
    using WPFgui.ViewModels;

    /// <summary>
    /// Bootstrapper Controller
    /// </summary>
    public class Bootstrapper : BootstrapperBase
    {
        /// <summary>
        /// Contructor
        /// </summary>
        public Bootstrapper()
        {
            Initialize();
        }

        /// <summary>
        /// Override of the view to display
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<ShellViewModel>();
        }
    }
}
